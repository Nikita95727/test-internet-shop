<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    $categories = App\category::all();
    $shopconfig = \App\shopconfig::find(1);
    return view('welcome', compact(array('categories', 'shopconfig')));
});

Route::get('/setLocale/{lang}', function ($lang) {
    session()->put('locale', $lang);
    return redirect()->back();
});

Route::get('/admin', 'AdminController@index');
Route::get('/shop', 'ShopController@index');
Route::get('/shop/buy/{product}', 'ShopController@buy');
Route::get('/shop/addtowishlist/{product}', 'ShopController@addtowishlist');
Route::get('/shop/cart', 'ShopController@cart');
Route::get('/shop/checkout/index', 'CheckoutController@index');

Route::get('shop/show/{product}', 'ProductController@show');
Route::get('/shop/product/edit/{product}', 'AdminController@editProduct');
Route::get('/shop/product/delete/{product}', 'ProductController@delete');
Route::post('/shop/product/addcomment', 'ProductController@addcomment');

// Маршруты для панели пользователя
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/dashboard/info', 'DashboardController@info');
Route::get('/dashboard/address', 'DashboardController@address');
Route::get('dashboard/address/createAddress', 'DashboardController@create');
Route::get('/dashboard/equal', 'DashboardController@equal');
Route::get('/dashboard/wishes', 'DashboardController@wishes');
Route::get('/shop/admin/showItems', 'AdminController@showItems');
Route::get('/shop/admin/createItem', 'AdminController@create');
Route::get('/shop/contacts', 'ShopController@contacts');
Route::post('/dashboard/changeAvatar', 'UserController@changeAvatar');
Route::post('/dashboard/changeName', 'UserController@changeName');
Route::post('/dashboard/changeEmail', 'UserController@changeEmail');
Route::post('/dashboard/addShippingAddress', 'ShippingAddressController@store');
Route::get('/dashboard/removeShippingAddress/{shippingAddress}', 'ShippingAddressController@remove');

// Маршруты для экспорта-импорта продуктов
Route::get('/shop/admin/export', 'ProductController@export');
Route::get('/admin/importproducts', 'AdminController@importProducts');

// Маршруты для админки
Route::get('/admin/catalog', 'AdminController@managecatalog');
Route::get('/shop/admin/managecustomers', 'AdminController@manageUsers');
Route::get('/shop/admin/editUser/{user}', 'AdminController@editUser');
Route::get('/shop/admin/createUser', 'AdminController@createUser');
Route::get('/shop/admin/showUsers', 'AdminController@showUsers');
Route::get('/shop/admin/users/import', 'AdminController@importUsers');
Route::get('/shop/admin/sales', 'AdminController@sales');

// Маршруты для взаимодействия с пользователями
Route::get('/shop/admin/users/export', 'UserController@export');
Route::get('/shop/admin/users/remove/{user}', 'UserController@remove');
Route::get('/shop/admin/users/showUser/{user}', 'UserController@show');
Route::post('/shop/admin/user/store', 'UserController@store');

// Маршруты для взаимодействия с ролями пользователя
Route::get('/shop/admin/user/manageRoles', 'AdminController@manageRoles');
Route::get('/shop/admin/user/remove/{role}', 'RoleController@remove');
Route::post('/shop/admin/user/storeRole', 'RoleController@store');
Route::post('/shop/admin/user/changeRole', 'UserController@changeRole');

// Маршруты для подарочной карты
Route::get('/shop/admin/infogiftcard', 'AdminController@infoGiftCard');
Route::get('/shop/admin/addgiftcard', 'AdminController@addGiftCard');
Route::get('/shop/deletegiftcard/{giftCard}','GiftCardController@deleteGiftCard');
Route::get('/shop/removegiftcard','GiftCardController@removeGiftCard');
Route::post('/shop/admin/savegiftcard', 'GiftCardController@storeGiftCard');
Route::post('/shop/verifygift', 'GiftCardController@verifyGiftCard');

// Маршруты для методов доставки
Route::get('/shop/admin/infoshippingmethods', 'AdminController@manageShippingMethods');
Route::get('/shop/admin/addshippingmethods', 'AdminController@createShippingMethods');
Route::get('/shop/admin/editshippingmethods/{shipping}', 'AdminController@updateShippingMethods');
Route::get('/shop/deleteshipping/{shipping}', 'ShippingMethodController@deleteShipping');
Route::post('/shop/admin/saveshipping', 'ShippingMethodController@store');

// Маршруты для купонов
Route::get('/shop/admin/addcupon', 'AdminController@addCupon');
Route::get('/shop/admin/infocupon', 'AdminController@infoCupon');
Route::get('/shop/deletecupon/{cupon}','CuponController@deleteCupon');   //передали модель
Route::post('/shop/admin/savecupon', 'CuponController@storeCupon');
Route::post('/shop/verifycupon', 'CuponController@verifyCupon');


Route::get('/shop/admin/managediscounts', 'AdminController@manageDiscounts');
Route::get('/shop/admin/managediscounts/{id}', 'ProductDiscountController@destroy');
Route::post('/shop/admin/managediscounts/store', 'ProductDiscountController@store');

// Маршруты для управления категориями
Route::get('/shop/admin/managecategories', 'AdminController@manageCategories');
Route::get('/shop/admin/createcategory', 'AdminController@createCategory');
Route::get('/shop/admin/deletecategory/{category}', 'CategoryController@deleteCategory');
Route::post('/shop/storeCategory', 'CategoryController@store');

// Маршруты для управления статичными страницами
Route::get('/admin/manageArticles', 'AdminController@manageArticles');
Route::get('/admin/manageArticles/create', 'AdminController@createArticle');
Route::get('/admin/manageArticles/edit/{id}', 'AdminController@editArticle');
Route::get('/admin/manageArticles/remove/{id}', 'StaticPageController@remove');
Route::get('/admin/manageArticles/changeStatus/{id}', 'StaticPageController@changeStatus');
Route::post('/admin/manageArticles/store', 'StaticPageController@store');

// Маршруты для управления слайдером
Route::get('/shop/admin/design', 'AdminController@managedesign');

// Маршруты для поиска и фильтрации по каталогу
Route::get('/shop/category/{category}', 'CategorySearch@categorySearch');
Route::get('/shop/sort/asc', 'CategorySearch@sortAsc');
Route::get('/shop/sort/desc', 'CategorySearch@sortDesc');

Route::post('/item/store','ProductController@store');
Route::post('/dashboard/address/create','ShippingAddressController@store');
Route::post('/item/import', 'ProductController@import');
Route::post('/user/import', 'UserController@import');

// Маршруты для корзины
Route::get('/shop/cart/addqty/{product}', 'CartController@addQty');
Route::get('/shop/cart/minusqty/{product}', 'CartController@minusQty');
Route::get('/shop/cart/changeQty/{id},{value}', 'CartController@changeQty');
Route::get('/shop/cart/removeProduct/{product}', 'CartController@removeProduct');
Route::get('/shop/cart/clearAll', 'CartController@clearAll');
Route::get('/shop/cart/addShippingMethod/{shippingMethod}', 'CartController@addShippingMethod');
Route::get('/shop/cart/getSubtotal', 'CartController@getSubtotal');
Route::get('/shop/cart/getTotal', 'CartController@getTotal');

// Маршруты для взаимодействия с листом сравнений
Route::get('/shop/comparelist/add/{product}', 'CompareListController@add');

// Маршруты для взаимодействия с листом пожеланий
Route::get('/shop/addtowishlist/{product}', 'WishlistController@addToWishlist');

// Маршруты для конфигурации магазина
Route::get('/shop/admin/storeconfigmenu', 'AdminController@storeConfigMenu');
Route::get('/shop/admin/storeconfig', 'AdminController@storeConfig');
Route::get('/shop/admin/managestores', 'AdminController@manageStores');
Route::get('/shop/admin/managestores/create', 'AdminController@createStore');
Route::get('/shop/admin/managestores/edit/{id}', 'AdminController@editStore');
Route::get('/shop/admin/managestores/delete/{id}', 'StoreController@delete');
Route::post('/shop/admin/managestores/store', 'StoreController@store');
Route::get('/shop/admin/managestores/editLocale/{id}', 'AdminController@editLocales');
Route::get('/shop/admin/managestores/deleteLocale/{id}', 'StoreController@deleteLocale');
Route::post('/shop/admin/managestores/addLocale', 'StoreController@addLocale');
Route::get('/shop/admin/managestores/changeLocaleStatus/{storeLocalId}/{storeId}', 'StoreController@changeStatus');
Route::get('/shop/admin/managestores/editCurrencies/{id}', 'AdminController@editCurrencies');
Route::get('/shop/admin/managestores/changeCurrencyStatus/{storeCurrencyId}/{storeId}', 'StoreController@changeCurrencyStatus');
Route::get('/shop/admin/managestores/setCurrentStore/{storeId}', 'StoreController@setCurrentStore');

// Маршруты для управления курсами валют
Route::get('/shop/admin/managecurrencies', 'AdminController@manageCurrencies');
Route::get('/shop/admin/managecurrencies/add', 'AdminController@addCurrency');
Route::get('/shop/admin/managecurrencies/updatecurrencies', 'CurrencyController@updateCurrencies');
Route::get('/shop/admin/managecurrrencies/remove/{id}', 'CurrencyController@remove');
Route::get('/shop/admin/managecurrencies/setAsDefault/{id}', 'CurrencyController@setDefault');
Route::post('/shop/admin/managecurrencies/save', 'CurrencyController@store');

// Маршруты для взаимодействия с производителями
Route::get('/admin/manageVendors', 'AdminController@manageVendors');
Route::get('/admin/deleteVendor/{vendor}', 'VendorController@deleteVendor');
Route::post('/admin/storeVendor', 'VendorController@storeVendor');

// Маршруты для взаимодействия с кэшем магазина
Route::get('/admin/manageCache', 'AdminController@manageCache');
Route::get('/admin/cacheClear', 'AdminController@clearCache');

Route::get('/admin/managelocale', 'AdminController@managelocale');
Route::get('/admin/managelocale/createlocale', 'AdminController@createlocale');
Route::get('/admin/managelocale/delete/{id}', 'LocaleController@destroy');
Route::post('/admin/managelocale/save', 'LocaleController@store');
