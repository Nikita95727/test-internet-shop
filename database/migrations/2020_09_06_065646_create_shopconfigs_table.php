<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopconfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopconfigs', function (Blueprint $table) {
            $table->increments('id');
            $table->char('shopName');
            $table->char('shopLogo', 255);
            $table->char('shopAddress');
            $table->char('shopEmail');
            $table->unsignedBigInteger('currency_id')
                ->default(1);
            $table->char('shopLocale')->default('EN');
            $table->timestamps();

            $table->foreign('currency_id')
                ->references('id')
                ->on('currencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopconfigs');
    }
}
