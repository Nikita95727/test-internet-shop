<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopconfigPhonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopconfig_phones', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('shopconfig_id');
            $table->char('shopConfigPhone');
            $table->timestamps();

            $table->foreign('shopconfig_id')->references('id')->on('shopconfigs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopconfig_phones');
    }
}
