<?php

use Illuminate\Database\Seeder;

class storeconfig extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $storeconfig = new App\shopconfig;
        $storeconfig->shopName = 'MirrorCart';
        $storeconfig->shopLogo = 'img/logo.jpg';
        $storeconfig->shopAddress = 'ул.Дмитрия Жукова 1';
        $storeconfig->shopEmail = 'dimadno228@hublo.com';
        $storeconfig->save();

        $phoneNumber = new \App\store_phone();
        $phoneNumber->store_id = $storeconfig->id;
        $phoneNumber->storePhone = '+380997765864';
        $phoneNumber->save();
    }
}
