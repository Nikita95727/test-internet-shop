<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    public function images()
    {
        return $this->hasMany('App\BannerImage', 'bannerId', 'id');
    }

    public function getImages()
    {
        return $this->images;
    }
}
