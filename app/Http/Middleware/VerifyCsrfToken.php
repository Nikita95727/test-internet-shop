<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/item/store',
        '/customer/create',
        '/customer/store',
        '/dashboard/address/create',
        '/item/import',
        '/user/import',
        '/shop/admin/savegiftcard',
        '/shop/verifygift',
        '/shop/admin/savecupon',
        '/shop/verifycupon',
        '/shop/storeCategory',
        '/shop/storeShopPhoneNumber',
        '/shop/storeShopName',
        '/shop/storeShopEmail',
        '/shop/storeShopLogo',
        '/shop/storeShopAddress',
        '/shop/admin/saveshipping',
        '/admin/storeVendor',
        '/shop/admin/user/storeRole',
        '/shop/admin/user/changeRole',
        '/dashboard/changeAvatar',
        '/dashboard/changeName',
        '/dashboard/changeEmail',
        '/dashboard/addShippingAddress',
        '/shop/product/addcomment',
        '/shop/admin/managecurrencies/save',
        '/shop/admin/design/storebanner',
        '/admin/managelocale/save',
        '/shop/admin/managediscounts/store',
        '/admin/manageArticles/store',
        '/shop/admin/managestores/store',
        '/shop/admin/managestores/addLocale'
    ];
}
