<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                return $next($request);
            }
        }

        return redirect('/');
    }

    private function _getUser()
    {
        return Auth::user();
    }
}
