<?php

namespace App\Http\Controllers;

use App\category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function store(CategoryRequest $request)
    {
        if ($request->input('categoryId')) {
            $id = $request->input('categoryId');
            $category = category::find($id);
            $this->_setCategory($category, $request);
            return redirect()->back()->with('success', __('Категория успешно изменена'));
        }

        $category = new category();
        $this->_setCategory($category, $request);
        return redirect()->back()->with('success', __('Категория успешно добавлена'));
    }

    public function deleteCategory($id)
    {
        category::find($id)->delete($id);
        return redirect()->back()->with('success', __('Категория удалена'));
    }

    private function _setCategory($category, $request)
    {
        $category->categoryName = $request->input('categoryName');
        $category->categoryCode = $request->input('categoryCode');
        if ($request->input('parentId')) {
            $category->parentId = $request->input('parentId');
        }
        $category->save();
    }
}
