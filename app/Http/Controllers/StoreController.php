<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRequest;
use App\shopconfig;
use App\store;
use App\storeAddress;
use App\storeCurrency;
use App\storeEmail;
use App\storeLocale;
use App\storePhone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('Admin');
    }

    public function store(StoreRequest $request)
    {
        $store = new store();
        $store->name = $request->post('storeName');
        $store->save();

        $storeAddress = new storeAddress();
        $storeAddress->store_id = $store->id;
        $storeAddress->address = $request->post('storeAddress');
        $storeAddress->save();

        foreach ($request->post('storeEmails') as $email) {
            $storeEmail = new storeEmail();
            $storeEmail->store_id = $store->id;
            $storeEmail->email = $email;
            $storeEmail->save();
        }

        foreach ($request->post('storePhones') as $phone) {
            $storePhone = new storePhone();
            $storePhone->store_id = $store->id;
            $storePhone->phone = $phone;
            $storePhone->save();
        }

        foreach ($request->post('shopLocales') as $shopLocale) {
            $storeLocale = new storeLocale();
            $storeLocale->store_id = $store->id;
            $storeLocale->locale_id = $shopLocale;
            $storeLocale->default = storeCurrency::NOT_DEFAULT_CURRENCY;
            $storeLocale->save();
        }

        foreach ($request->post('shopCurrencies') as $shopCurrency) {
            $storeCurrency = new storeCurrency();
            $storeCurrency->store_id = $store->id;
            $storeCurrency->currency_id = $shopCurrency;
            $storeCurrency->default = storeCurrency::NOT_DEFAULT_CURRENCY;
            $storeCurrency->save();
        }

        return redirect('/shop/admin/managestores');
    }

    public function addLocale(Request $request)
    {
        foreach ($request->post('shopLocales') as $storeLocal) {
            $storeLocale = new storeLocale();
            $storeLocale->store_id = $request->post('store_id');
            $storeLocale->locale_id = $storeLocal;
            $storeLocale->default = storeCurrency::NOT_DEFAULT_CURRENCY;
            $storeLocale->save();
        }

        return redirect()->back();
    }

    public function changeStatus($storeLocalId, $storeId)
    {
        $storeLocales = store::find($storeId)
            ->getLocales();

        foreach ($storeLocales as $storeLocale) {
            $storeLocale->default = storeLocale::NOT_DEFAULT_LOCALE;
            $storeLocale->save();
        }

        $storeLocale = storeLocale::find($storeLocalId);
        $storeLocale->default = !$storeLocale->default;
        $storeLocale->save();

        return redirect()->back();
    }

    public function changeCurrencyStatus($storeCurrencyId, $storeId)
    {
        $storeCurrencies = store::find($storeId)
            ->getCurrencies();

        foreach ($storeCurrencies as $storeCurrency) {
            $storeCurrency->default = storeCurrency::NOT_DEFAULT_CURRENCY;
            $storeCurrency->save();
        }

        $storeCurrency = storeCurrency::find($storeCurrencyId);
        $storeCurrency->default = !$storeCurrency->default;
        $storeCurrency->save();

        return redirect()->back();
    }

    public function deleteLocale($id)
    {
        $storeLocale = storeLocale::find($id)
            ->delete();

        return redirect()->back();
    }

    public function setCurrentStore($storeId)
    {
        $stores = store::all();

        foreach ($stores as $store) {
            $store->current = store::NOT_CURRENT_STORE;
            $store->save();
        }

        $store = store::find($storeId);
        $store->current = store::CURRENT_STORE_VALUE;
        $store->save();

        return redirect()->back();
    }

    public function delete($id)
    {
        storeAddress::where('store_id', $id)
            ->delete();

        storeEmail::where('store_id', $id)
            ->delete();

        storePhone::where('store_id', $id)
            ->delete();

        storeLocale::where('store_id', $id)
            ->delete();

        storeCurrency::where('store_id', $id)
            ->delete();

        store::find($id)
            ->delete();

        return redirect()->back();
    }
}
