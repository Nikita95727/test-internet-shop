<?php

namespace App\Http\Controllers;

use App\shippingMethod;
use Illuminate\Http\Request;

class ShippingMethodController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'shippingMethodName' => 'required',
            'shippingMethodDescription' => 'required',
            'shippingMethodPrice' => 'required',
            'shippingMethodStatus' => 'required'
        ]);

        if ($request->post('shippingMethod_id')) {
            $shippingMethod = shippingMethod::find($request->post('shippingMethod_id'));
            $this->_setShippingMethod($request, $shippingMethod);
            return redirect('/shop/admin/infoshippingmethods')->with('success', 'Метод доставки успешно изменён');
        }

        $shippingMethod = new shippingMethod();
        $this->_setShippingMethod($request, $shippingMethod);
        return redirect('/shop/admin/infoshippingmethods')->with('success', 'Метод доставки успешно добавлен');
    }

    public function deleteShipping($id)
    {
        shippingMethod::find($id)
            ->delete();

        return redirect()->back();
    }

    private function _setShippingMethod($request, $shippingMethod)
    {
        $shippingMethod->shippingMethodName = $request->post('shippingMethodName');
        $shippingMethod->shippingMethodDescription = $request->post('shippingMethodDescription');
        $shippingMethod->shippingMethodPrice = $request->post('shippingMethodPrice');
        $shippingMethod->shippingMethodStatus = $request->post('shippingMethodStatus');
        $shippingMethod->save();
    }
}
