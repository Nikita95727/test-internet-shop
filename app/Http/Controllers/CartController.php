<?php

namespace App\Http\Controllers;

use App\cart;
use App\shippingMethod;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function getSubtotal()
    {
        $cart = new cart();
        return response()->json($cart->getSubtotal());
    }

    public function getTotal()
    {
        $cart = new cart();
        return response()->json($cart->getTotal());
    }

    public function addQty($id)
    {
        $cart = $this->_getSession();
        $cart[$id]['qty'] = $cart[$id]['qty']+1;
        session()->put('cart', $cart);
        //return redirect()->back();
        return response()->json($cart[$id]['qty']);
    }

    public function changeQty($id, $value)
    {
        $cart = $this->_getSession();
        $cart[$id]['qty'] = $value;

        if ($cart[$id]['qty'] == 0) {
            unset($cart[$id]);
        }

        session()->put('cart', $cart);
        return response()->json($cart[$id]['qty']);
    }

    public function minusQty($id)
    {
        $cart = $this->_getSession();
        $cart[$id]['qty'] = $cart[$id]['qty']-1;

        if ($cart[$id]['qty'] == 0) {
            unset($cart[$id]);
        }

        session()->put('cart', $cart);
        //return redirect()->back();
        return response()->json($cart[$id]['qty']);
    }

    public function clearAll()
    {
        if (session()->exists('cart')) {
            session()->forget('cart');
            return redirect()->back()->with('success', __('Корзина пуста'));
        }

        return redirect()->back()->with('error', __('Произошла ошибка'));
    }

    public function addShippingMethod($id)
    {
        $shippingMethod = shippingMethod::find($id);
        session()->put('shippingMethodPrice', $shippingMethod->shippingMethodPrice);
        return response()->json($shippingMethod->shippingMethodPrice);
    }

    public function removeProduct($id)
    {
        $cart = new cart();
        $cart->removeProduct($id);

        return redirect()->back();
    }

    private function _getSession()
    {
        return session()->get('cart');
    }
}
