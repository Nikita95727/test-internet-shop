<?php

namespace App\Http\Controllers;

use App\product;
use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Auth;
use App\shopconfig;
use Illuminate\Support\Facades\Artisan;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('Admin');
    }

    public function index()
    {
        return view('admin.index');
    }

    public function managecatalog()
    {
        return view('admin.products.index');
    }

    public function showItems()
    {
        $products = App\product::paginate(3);
        return view('admin.products.showproducts',compact('products'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vendors = App\vendor::all();
        $categories = App\category::all();
        return view('admin.products.createproduct', compact(
            array('categories', 'vendors')
        ));
    }

    public function editProduct($id)
    {
        $product = App\product::find($id);
        $vendors = App\vendor::all();
        $categories = App\category::all();
        return view('admin.products.editproduct', compact(
            array('categories', 'vendors', 'product')
        ));
    }

    public function manageUsers()
    {
        return view('admin.users.manage');
    }

    public function createUser()
    {
        return view('admin.users.createuser');
    }

    public function editUser($id)
    {
        $user = App\User::find($id);
        return view('admin.users.edituser', compact('user'));
    }

    public function showUsers()
    {
        $users = App\User::paginate(3);
        return view('admin.users.showusers', compact('users'));
    }

    public function importUsers()
    {
        return view('admin.users.importusers');
    }

    public function manageRoles()
    {
        $roles = App\role::all();
        return view('admin.users.roles.showroles', compact('roles'));
    }

    public function importProducts()
    {
        return view('admin.products.importproducts');
    }

    public function sales()
    {
        return view('admin.sales');
    }

    public function infoGiftCard()
    {
        $giftcards = App\giftCard::paginate(3);
        return view('admin.giftcard.infogiftcard', compact('giftcards'));
    }

    public function addGiftCard()
    {
        return view('admin.giftcard.addgiftcard');
    }

    public function manageShippingMethods()
    {
        $shippments = App\shippingMethod::paginate(3);
        return view('admin.shippingMethods.infoshippingmethods', compact('shippments'));
    }

    public function createShippingMethods()
    {
        return view('admin.shippingMethods.addshippingmethods');
    }

    public function updateShippingMethods($id)
    {
        $shippingMethod = App\shippingMethod::find($id);
        return view('admin.shippingMethods.editshippingmethods', compact('shippingMethod'));
    }

    public function infoCupon()
    {
        $cupons = App\cupon::paginate(3);
        return view('admin.cupon.infocupon', compact('cupons'));
    }

    public function addCupon()
    {
        return view('admin.cupon.addcupon');
    }

    public function manageCategories()
    {
        $categories = App\category::paginate(3);
        return view('admin.categories.infocategories', compact('categories'));
    }

    public function storeConfigMenu()
    {
        return view('admin.storeconfig.configmenu');
    }

    public function storeConfig()
    {
        $shopconfig = shopconfig::find(1);
        return view('admin.storeconfig.infostoreconfig', compact('shopconfig'));
    }

    public function manageStores()
    {
        $stores = App\store::all();

        return view('admin.storeconfig.managestores', compact('stores'));
    }

    public function createStore()
    {
        return view('admin.storeconfig.createstore');
    }

    public function editStore($id)
    {
        $store = App\store::find($id);

        return view('admin.storeconfig.editstore', compact('store'));
    }

    public function editLocales($id)
    {
        $store = App\store::find($id);

        return view('admin.storeconfig.editlocales', compact('store'));
    }

    public function editCurrencies($id)
    {
        $store = App\store::find($id);

        return view('admin.storeconfig.editcurrencies', compact('store'));
    }

    public function manageCurrencies()
    {
        $currencies = App\Currency::paginate(3);
        return view('admin.currency.managecurrencies', compact('currencies'));
    }

    public function addCurrency()
    {
        return view('admin.currency.addcurrency');
    }

    public function manageVendors()
    {
        $vendors = App\vendor::all();
        return view('admin.vendor.infovendor', compact('vendors'));
    }

    public function manageCache()
    {
        return view('admin.cache.manage');
    }

    public function clearCache()
    {
        Artisan::call('cache:clear');
        Artisan::call('config:cache');
        Artisan::call('view:clear');
        Artisan::call('route:clear');
        Artisan::call('backup:clean');
        return redirect()->back()->with('success', 'Кэш успешно очищен');
    }

    public function vuejs()
    {
        $configs = shopconfig::all();
        return view('admin.vuejs', compact('configs'));
    }

    public function managelocale()
    {
        $locales = App\Locale::all();

        return view('admin.locale.managelocales', compact('locales'));
    }

    public function createlocale()
    {
        return view('admin.locale.addlocale');
    }

    public function managedesign()
    {
        return view('admin.design.index');
    }

    public function addbanner()
    {
        return view('admin.design.createbanner');
    }

    public function manageDiscounts()
    {
        $products = product::all();
        $discounts = App\ProductDiscount::all();

        return view('admin.discounts.managediscounts', compact(array('discounts', 'products')));
    }

    public function manageArticles()
    {
        $articles = App\StaticPage::all();

        return view('admin.articles.managearticles', compact('articles'));
    }

    public function createArticle()
    {
        return view('admin.articles.createarticle');
    }

    public function editArticle($id)
    {
        $article = App\StaticPage::find($id);

        return view('admin.articles.editarticle', compact('article'));
    }
}
