<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductDiscountRequest;
use App\ProductDiscount;
use Illuminate\Http\Request;

class ProductDiscountController extends Controller
{
    public function store(ProductDiscountRequest $request)
    {
        $productDiscount = new ProductDiscount();
        $productDiscount->product_id = $request->post('product');
        $productDiscount->discountValue = $request->post('discountValue');
        $productDiscount->expirationDate = $request->post('expirationDate');
        $productDiscount->save();

        return redirect('/shop/admin/managediscounts')->with('success', 'admin.discount_added_msg');
    }

    public function destroy($id)
    {
        ProductDiscount::find($id)
            ->delete();

        return redirect('/shop/admin/managediscounts')->with('success', 'admin.discount_delete_msg');
    }
}
