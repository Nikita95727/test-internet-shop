<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'itemTitle' => 'required',
            'itemDescription' => 'required',
            'itemPicture' => 'required',
            'itemMaterial' => 'required'
        ]);
        $item = new App\Items;
        $item->itemTitle = $request->input('itemTitle');
        $item->itemDescription = $request->input('itemDescription');
        $item->itemPicture = $request->input('itemPicture');
        $item->matherial = $request->input('itemMaterial');
        $item->itemPrice = $item->setFinalPrice($request->input('itemMaterial'));
        $item->save();
        return redirect('/admin')->with('success','Item had been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = App\Items::find($id);
        return view('shop.show',compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id){
        App\Items::find($id)->delete();
        return redirect()->back();
    }
}
