<?php

namespace App\Http\Controllers;

use App\Http\Requests\LocaleRequest;
use App\Locale;
use Illuminate\Http\Request;

class LocaleController extends Controller
{
    public function store(LocaleRequest $request)
    {
        if ($request->post('localeId')) {
            $locale = Locale::find($request->post('localeId'));
            $this->_setLocale($locale, $request);

            return redirect('/admin/managelocale')->with('success', 'admin.locale_edit_success_message');
        }

        $locale = new Locale();
        $this->_setLocale($locale, $request);

        return redirect('/admin/managelocale')->with('success', 'admin.locale_add_success_message');
    }

    public function destroy($id)
    {
        Locale::find($id)
            ->delete();

        return redirect('/admin/managelocale')->with('succcess', 'admin.locale_deleted_success_message');
    }

    private function _setLocale($locale, $request)
    {
        $locale->localeName = $request->post('localeName');
        $locale->localeCode = $request->post('localeCode');

        if ($request->file('localeFlag')) {
            $locale->localeFlag = $request->file('localeFlag')
                ->store('uploads', 'public');
        }

        $locale->save();
    }
}
