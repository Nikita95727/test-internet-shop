<?php

namespace App\Http\Controllers;

use App\product;
use App\wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishlistController extends Controller
{
    public function __construct()
    {
        $this->middleware = 'auth';
    }

    public function addToWishlist($id)
    {
        return redirect()->back()->with('success', __('Продукт успешно добавлен в список желаний'));
    }
}
