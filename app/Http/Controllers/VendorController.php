<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class VendorController extends Controller
{
    public function storeVendor(Request $request)
    {
        $this->validate($request, [
            'vendorName' => 'required',
        ]);

        if ($request->input('vendorId')) {
            $id = $request->input('vendorId');
            $vendor = App\vendor::find($id);
            $vendor->vendor_name = $request->input('vendorName');
            $vendor->save();
            return redirect()->back()->with('success', __('Производитель успешно изменён'));
        }

        $vendor = new App\vendor();
        $vendor->vendor_name = $request->input('vendorName');
        $vendor->save();
        return redirect()->back()->with('success', __('Производитель успешно добавлен'));
    }

    public function deleteVendor($id)
    {
        App\vendor::find($id)->delete();
        return redirect()->back()->with('success', __('Производитель успешно удалён'));
    }
}
