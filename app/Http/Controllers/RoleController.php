<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\role;

class RoleController extends Controller
{
    public function remove($id)
    {
        role::find($id)->delete();
        return redirect()->back('success', __('Роль успешно удалена'));
    }

    public function store(Request $request)
    {
        $this->validate($request, array(
            'roleName' => 'required'
        ));

        $role = new role();
        $role->roleName = $request->post('roleName');
        $role->save();
        return redirect()->back()->with('success', __('Роль успешно добавлена'));
    }
}
