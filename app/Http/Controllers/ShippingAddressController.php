<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ShippingAddressRequest;

class ShippingAddressController extends Controller
{

    public function remove($id)
    {
        App\user_address::find($id)
            ->delete();

        return redirect('/dashboard/address')->with('success', __('Адрес успешно удалён'));
    }

    public function store(ShippingAddressRequest $request)
    {
        $address = new App\user_address();
        $this->_setAddress($request, $address);

        $postcode = new App\user_postcode();
        $this->_setPostCode($request, $postcode);

        $telephone = new App\user_telephone();
        $this->_setTelephone($request, $telephone);

        $city = new App\user_city();
        $this->_setUserCity($request, $city);

        return redirect('/dashboard/address')->with('success', __('Адрес успешно добавлен'));
    }

    private function _setAddress($request, $address)
    {
        $address->user_id = $request->post('userId');
        $address->address = $request->post('userAddress');
        $address->save();
    }

    private function _setPostCode($request, $postcode)
    {
        $postcode->user_id = $request->post('userId');
        $postcode->postcode = $request->post('userTelephone');
        $postcode->save();
    }

    private function _setTelephone($request, $telephone)
    {
        $telephone->user_id = $request->post('userId');
        $telephone->telephone = $request->post('userTelephone');
        $telephone->save();
    }

    private function _setUserCity($request, $city)
    {
        $city->user_id = $request->post('userId');
        $city->cityName = $request->post('userCity');
        $city->save();
    }
}
