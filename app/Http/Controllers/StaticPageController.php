<?php

namespace App\Http\Controllers;

use App\StaticPage;
use Illuminate\Http\Request;

class StaticPageController extends Controller
{
    public function remove($id)
    {
        StaticPage::find($id)
            ->delete();

        return redirect('/admin/manageArticles');
    }

    public function changeStatus($id)
    {
        $article = StaticPage::find($id);
        $article->status = !$article->status;
        $article->save();

        return redirect('/admin/manageArticles');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
           'articleTitle' => 'required',
           'articleDescription' => 'required',
           'articleStatus' => 'required'
        ]);

        if ($request->post('article_id')) {
            $article = StaticPage::find($request->post('article_id'));
            $this->_setArticle($article, $request);

            return redirect('/admin/manageArticles')->with('success', 'admin.article_edited_msg');
        }

        $article = new StaticPage();
        $this->_setArticle($article, $request);

        return redirect('/admin/manageArticles')->with('success', 'admin.article_created_msg');
    }

    private function _setArticle($article, $request)
    {
        $article->title = $request->post('articleTitle');
        $article->description = $request->post('articleDescription');
        $article->status = $request->post('articleStatus');
        $article->save();
    }
}
