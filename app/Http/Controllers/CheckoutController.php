<?php

namespace App\Http\Controllers;

use App\cart;
use App\shippingMethod;
use Illuminate\Http\Request;use function Symfony\Component\String\u;

class CheckoutController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkout');
    }

    public function index()
    {
        $cart = new cart();
        $product = session()->get('product');
        $shippingMethods = shippingMethod::where('shippingMethodStatus', shippingMethod::AVALIABLE_SHIPPING_METHOD)->get();
        return view('shop.checkout.index', compact([
            'cart', 'shippingMethods', 'product'
        ]));
    }
}
