<?php

namespace App\Http\Controllers;

use App\Currency;
use App\Http\Requests\CurrencyRequest;
use App\shopconfig;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    public function store(CurrencyRequest $request)
    {
        if ($request->post('currency_id')) {
            $currency = Currency::find($request->post('currency_id'));
            $this->_setCurrency($currency, $request);
            return redirect('/shop/admin/managecurrencies')->with('success', __('Валюта успешно изменена'));
        }

        $currency = new Currency();
        $this->_setCurrency($currency, $request);
        return redirect('/shop/admin/managecurrencies')->with('success', __('Валюта успешно добавлена'));
    }

    public function remove($id)
    {
        Currency::find($id)->delete();
        return redirect()->back()->with('success', __('Валюта успішно видалена'));
    }

    public function updateCurrencies()
    {
        $client = new Client();
        $rates = json_decode($client->request('GET', Currency::CURRENCIES_UPDATE_URL)
            ->getBody()
            ->getContents());

        foreach ($rates as $rate) {
            $currencies = Currency::where('currencyCode', $rate->ccy)
                ->get();
            foreach ($currencies as $currency) {
                $currency->currencyValue = $rate->sale;
                $currency->save();
            }
        }

        return redirect('/shop/admin/managecurrencies')->with('success', __('Курсы валют успешно обновлены'));
    }

    public function setDefault($id)
    {
        $currencies = Currency::all();
        foreach ($currencies as $currency) {
            $currency->default = Currency::NOT_DEFAULT_CURRENCY;
            $currency->save();
        }

        $currency = Currency::find($id);
        $currency->default = !$currency->default;
        $currency->save();

        $shopConfig = shopconfig::find(1);
        $shopConfig->currency_id = $currency->id;
        $shopConfig->save();

        return redirect('/shop/admin/managecurrencies');
    }

    private function _setCurrency($currency, $request)
    {
        $currency->currencyName = $request->post('currencyName');
        $currency->currencyCode = $request->post('currencyCode');
        $currency->currencySymbol = $request->post('currencySymbol');
        $currency->currencyValue = $request->post('currencyValue');
        $currency->currencyStatus = $request->post('currencyStatus');
        $currency->save();
    }
}
