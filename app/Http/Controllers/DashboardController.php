<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('dashboard.index');
    }

    public function info()
    {
        return view('dashboard.info');
    }

    public function address()
    {
        $shippingAddresses = App\shippingAddress::all();
        return view('dashboard.address', compact('shippingAddresses'));
    }

    public function create()
    {
        return view('dashboard.createAddress');
    }

    public function equal()
    {
        /*if (Auth::check()) {
            $compareListItems = Auth::user()->getCompareList();
             return view('dashboard.equal', compact('compareListItems'));
        }

        return redirect('login');*/
    }

    public function wishes()
    {
        //$wishlistItems = App\wishlist::where('user_id', Auth::user()->id)->get();
        return view('dashboard.wishes');
    }
}
