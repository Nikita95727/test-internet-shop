<?php

namespace App\Http\Controllers;

use App\Banner;
use App\BannerImage;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, array(
            'bannerName' => 'required',
            'bannerImages' => 'required'
        ));

        if ($request->post('bannerId')) {
            $banner = Banner::find($request->post('bannerId'));
            $this->_setBanner($banner, $request);

            return redirect('/shop/admin/design')->with(__('admin.banner_successfully_edited'));
        }

        $banner = new Banner();
        $this->_setBanner($banner, $request);
        return redirect('/shop/admin/design')->with(__('admin.banner_successfully_added'));
    }

    private function _setBanner($banner, $request)
    {
        $banner->bannerName = $request->post('bannerName');
        $banner->save();
        $bannerImages = $request->file('bannerImages');
        foreach ($bannerImages as $image) {
            $bannerImage = new BannerImage();
            $bannerImage->bannerId = $banner->id;
            $bannerImage->bannerImage = $image->store('uploads', 'public');
            $bannerImage->save();
        }
    }
}
