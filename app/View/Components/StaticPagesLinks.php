<?php

namespace App\View\Components;

use App\StaticPage;
use Illuminate\View\Component;

class StaticPagesLinks extends Component
{
    public $staticPages;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->staticPages = $this->_getStaticPages();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.static-pages-links');
    }

    private function _getStaticPages()
    {
        return StaticPage::all();
    }
}
