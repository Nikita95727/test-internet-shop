<?php

namespace App\View\Components;

use App\shopconfig;
use Illuminate\View\Component;
use App\product;

class ResentArrival extends Component
{
    public $latestProducts;
    public $storeCurrencySymbol;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->latestProducts = product::latest()
            ->take(8)
            ->get();

        $this->storeCurrencySymbol = $this->getStoreCurrencySymbol();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.resent-arrival');
    }

    private function getStoreCurrencySymbol()
    {
        return shopconfig::find(1)
            ->getCurrencySymbol();
    }
}
