<?php

namespace App\View\Components;

use App\Locale;
use Illuminate\View\Component;

class AdminTopBar extends Component
{
    public $locales;
    public $currentLocale;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->currentLocale = Locale::find(1);
        $this->locales = Locale::all();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.admin-top-bar');
    }
}
