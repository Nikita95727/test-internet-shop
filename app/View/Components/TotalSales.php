<?php

namespace App\View\Components;

use App\Order;
use App\shopconfig;
use Illuminate\Support\Facades\Config;
use Illuminate\View\Component;

class TotalSales extends Component
{
    public $totalSales;
    public $currencySymbol;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $order = new Order();
        $this->totalSales = round(($order
            ->countTotalSales() / $this->_getStoreModel()
                ->getCurrencyValue()), 2);

        $this->currencySymbol = $this->_getStoreModel()
            ->getCurrencySymbol();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.total-sales');
    }

    private function _getStoreModel()
    {
        $shopConfig = shopconfig::find(1);

        return $shopConfig;
    }
}
