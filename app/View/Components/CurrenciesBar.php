<?php

namespace App\View\Components;

use App\Currency;
use Illuminate\View\Component;

class CurrenciesBar extends Component
{
    public $currencies;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->currencies = $this->_getCurrencies();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.currencies-bar');
    }

    private function _getCurrencies()
    {
        return Currency::all();
    }
}
