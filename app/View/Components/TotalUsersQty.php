<?php

namespace App\View\Components;

use App\User;
use Illuminate\View\Component;

class TotalUsersQty extends Component
{
    public $qty;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->qty = User::all()
            ->count();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.total-users-qty');
    }
}
