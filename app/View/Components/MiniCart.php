<?php

namespace App\View\Components;

use App\cart;
use App\shopconfig;
use Illuminate\View\Component;

class MiniCart extends Component
{
    public $cartItems;
    public $cart;
    public $storeCurrencySymbol;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->cartItems = $this->_getCartItems();
        $this->cart = $this->_getCartModel();
        $this->storeCurrencySymbol = $this->_getStoreCurrencySymbol();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.mini-cart');
    }

    private function _getCartItems()
    {
        return session('cart');
    }

    private function _getCartModel()
    {
        return new cart();
    }

    private function _getStoreCurrencySymbol()
    {
        return shopconfig::find(1)
            ->getCurrencySymbol();
    }
}
