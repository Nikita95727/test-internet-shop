<?php

namespace App\View\Components;

use App\Order;
use Illuminate\View\Component;

class TotalOrdersQty extends Component
{
    public $qty;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->qty = Order::all()
            ->count();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.total-orders-qty');
    }
}
