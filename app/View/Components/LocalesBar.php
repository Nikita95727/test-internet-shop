<?php

namespace App\View\Components;

use App\Locale;
use Illuminate\View\Component;

class LocalesBar extends Component
{
    public $locales;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->locales = $this->_getLocales();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.locales-bar');
    }

    private function _getLocales()
    {
        return Locale::all();
    }
}
