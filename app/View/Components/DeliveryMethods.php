<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\shippingMethod;

class DeliveryMethods extends Component
{
    public $shippingMethods;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->shippingMethods = $this->_getShippingMethods();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.delivery-methods');
    }

    private function _getShippingMethods()
    {
        return shippingMethod::all();
    }
}
