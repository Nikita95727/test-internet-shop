<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDiscount extends Model
{
    public function product()
    {
        return $this->hasOne('App\product', 'id', 'product_id');
    }

    public function getProductTitle()
    {
        return $this->product->getTitle();
    }
}
