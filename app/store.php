<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class store extends Model
{
    const CURRENT_STORE_VALUE = 1;
    const NOT_CURRENT_STORE = 0;

    public function currencies()
    {
        return $this->hasMany('App\storeCurrency','store_id');
    }

    public function phone()
    {
        return $this->hasMany('App\storePhone','store_id');
    }

    public function locales()
    {
        return $this->hasMany('App\storeLocale', 'store_id');
    }

    public function address()
    {
        return $this->hasOne('App\storeAddress', 'store_id');
    }

    public function email()
    {
        return $this->hasMany('App\storeEmail', 'store_id');
    }

    public function getLocales()
    {
        return $this->locales;
    }

    public function getEmails()
    {
        return $this->email;
    }

    public function getPhones()
    {
        return $this->phone;
    }

    public function getAddress()
    {
        return $this->address->address;
    }

    public function getCurrencies()
    {
        return $this->currencies;
    }
/*
    public function getCurrencyValue()
    {
        return $this->currency->getCurrencyValue();
    }

    public function getCurrencySymbol()
    {
        return $this->currency->getCurrencySymbol();
    }*/
}
