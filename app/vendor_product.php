<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vendor_product extends Model
{
    public function vendor()
    {
        return $this->hasOne('App\vendor', 'id', 'vendor_id');
    }

    public function getVendor()
    {
        return $this->vendor->vendor_name;
    }
}
