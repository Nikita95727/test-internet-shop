<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class storeCurrency extends Model
{
    const NOT_DEFAULT_CURRENCY = 0;
    const DEFAULT_CURRENCY = 1;

    public function currency()
    {
        return $this->hasOne('App\Currency', 'id', 'currency_id');
    }

    public function getCurrencyName()
    {
        return $this->currency->currencyName;
    }

    public function getCurrencyCode()
    {
        return $this->currency->currencyCode;
    }

    public function getCurrencyValue()
    {
        return $this->currency->currencyValue;
    }

    public function getCurrencySymbol()
    {
        return $this->currency->currencySymbol;
    }
}
