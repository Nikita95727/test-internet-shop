<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function countTotalSales()
    {
        $orders = Order::all()
            ->pluck('totalPrice');

        $totalSales = 0;

        foreach ($orders as $order) {
            $totalSales += $order;
        }

        return $totalSales;
    }
}
