<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class shopconfig extends Model
{
    public function currency()
    {
        return $this->hasOne('App\Currency', 'id', 'currency_id');
    }

    public function getCurrencyValue()
    {
        return $this->currency->currencyValue;
    }

    public function getCurrencySymbol()
    {
        return $this->currency->currencySymbol;
    }
}
