<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category_product extends Model
{
    public function category()
    {
        return $this->hasOne('App\category','category_id', 'id');
    }

    public function getCategoryName()
    {
        return $this->category->categoryName;
    }
}
