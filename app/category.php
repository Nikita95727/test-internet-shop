<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    const FIRST_LEVEL_CATEGORY_IDENTIFICATOR = '*';

    public function parentCategory()
    {
        return $this->hasOne('App\Category', 'id', 'parentId');
    }

    public function getParentCategoryName()
    {
        if (!empty($this->parentCategory->categoryName)) {
            return $this->parentCategory->categoryName;
        }

        return self::FIRST_LEVEL_CATEGORY_IDENTIFICATOR;
    }

    public function categoryChildren()
    {
        return $this->hasMany('App\Category', 'id', 'parentId');
    }
}
