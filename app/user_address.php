<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_address extends Model
{
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function getUser()
    {
        return $this->user;
    }
}
