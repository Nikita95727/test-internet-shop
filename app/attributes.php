<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Models\AttributeValue;

class attributes extends Model
{
    protected $fillable = [
        'code', 'name', 'frontend_type', 'is_filterable', 'is_required'
    ];

    protected $casts  = [
        'is_filterable' =>  'boolean',
        'is_required'   =>  'boolean',
    ];

    public function values()
    {
        return $this->hasMany(AttributeValue::class);
    }
}
