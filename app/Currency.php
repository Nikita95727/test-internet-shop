<?php

namespace App;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    const CURRENCIES_UPDATE_URL = 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5';
    const NOT_DEFAULT_CURRENCY = 0;
}
