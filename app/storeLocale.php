<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class storeLocale extends Model
{
    const NOT_DEFAULT_LOCALE = 0;
    const DEFAULT_LOCALE = 1;

    public function locale()
    {
        return $this->hasOne('App\Locale', 'id', 'locale_id');
    }

    public function getLocaleCode()
    {
        return $this->locale->localeCode;
    }

    public function getLocaleName()
    {
        return $this->locale->localeName;
    }
}
