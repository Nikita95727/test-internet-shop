<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\comments;

class product extends Model
{
    protected $fillable = ['image', 'title', 'description', 'price', 'categories_id', 'vendor_id'];

    public function attributes()
    {
        return $this->hasMany('App\attributes', 'id');
    }

    public function category()
    {
        return $this->hasOne('App\category_product', 'product_id', 'id');
    }

    public function vendor()
    {
        return $this->hasOne('App\vendor_product', 'product_id');
    }

    public function image()
    {
        return $this->hasMany('App\images', 'products_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany('App\comments', 'product_id');
    }

    public function discount()
    {
        return $this->hasOne('App\ProductDiscount', 'product_id');
    }

    public function getDiscount()
    {
        if (!empty($this->discount->discountValue)) {
            return $this->discount->discountValue;
        }

        return 0;
    }

    public function getComments()
    {
        return $this->comments;
    }

    public function setCategory($category)
    {
    	$this->category = $category;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setVendor($vendor)
    {
    	$this->vendor = $vendor;
    }

    public function getVendor()
    {
    	//return $this->vendor->getVendor();
    }

    public function setImage($image)
    {
    	$this->image = $image;
    }

    public function setTitle($title)
    {
    	$this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getShortDescription()
    {
        return substr($this->getDescription(), 0, '255');
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getPrice()
    {
        if ($this->getDiscount()) {
            return round(($this->price - (($this->price * $this->getDiscount()) / 100)) / $this->getStoreCurrencyValue(), 2);
        }

        return round(($this->price / $this->getStoreCurrencyValue()), 2);
    }

    public function getProductsByCategory($category)
    {
        return $this::where('category_code', $category)->get();
    }

    private function getStoreCurrencyValue()
    {
        return shopconfig::find(1)
            ->getCurrencyValue();
    }
}
