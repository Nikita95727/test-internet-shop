<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'email', 'customer_birthdate', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    private const ADMIN_GROUP_ID = 1;
    private const CONTENT_MANAGER_GROUP_ID = 2;

    public function wishlist()
    {
        return $this->hasOne('App\wishlist');
    }

    public function role()
    {
        return $this->hasOne('App\role','id','role_id');
    }

    public function comparelist()
    {
        return $this->hasMany('App\CompareList', 'user_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany('App\comments', 'user_id', 'id');
    }

    public function getCompareList()
    {
        return $this->comparelist;
    }


    public function address()
    {
        return $this->hasMany('App\user_address', 'user_id', 'id');
    }

    public function postcode()
    {
        return $this->hasOne('App\user_postcode', 'user_id', 'id');
    }

    public function telephone()
    {
        return $this->hasOne('App\user_telephone', 'user_id', 'id');
    }

    public function city()
    {
        return $this->hasOne('App\user_city', 'user_id', 'id');
    }

    public function getRoleName()
    {
        return $this->role->roleName;
    }

    public function hasAccess()
    {
        if (($this->role->id === self::ADMIN_GROUP_ID) || ($this->role->id === self::CONTENT_MANAGER_GROUP_ID)) {
            return true;
        }

        return false;
    }

    public function isContentManager()
    {
        if ($this->role->id === self::CONTENT_MANAGER_GROUP_ID) {
            return true;
        }

        return false;
    }

    public function isAdmin()
    {
        if ($this->role->id === self::ADMIN_GROUP_ID) {
            return true;
        }

        return false;
    }


    public function getUserRoles()
    {
        return role::all();
    }

    public function getUserAddresses()
    {
        if (!empty($this->address)) {
            return $this->address;
        }

        return 0;
    }

    public function getUserTelephone()
    {
        if (!empty($this->telephone->telephone)) {
            return $this->telephone->telephone;
        }

        return 0;
    }

    public function getUserPostCode()
    {
        if (!empty($this->postcode->postcode)) {
            return $this->postcode->postcode;
        }

        return 0;
    }

    public function getUserCity()
    {
        if (!empty($this->city->cityName)) {
            return $this->city->cityName;
        }

        return 0;
    }

    public function getComments()
    {
        return $this->comments;
    }
}
