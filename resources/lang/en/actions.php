<?php

return array(
  'close' => 'Close',
  'save' => 'Save',
  'actions' => 'Actions',
  'add' => 'Add',
  'create' => 'Create',
  'change_locales' => 'Change locales',
  'change_currencies' => 'Change currencies',
  'add_phone' => 'Add phone',
  'add_email' => 'Add email'
);
