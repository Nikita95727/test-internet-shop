<?php

return array(
  'name' => 'Name',
  'surname' => 'Surname',
  'birthdate' => 'Birthdate',
  'email' => 'E-mail',
  'actions' => 'Actions',
  'address' => 'Address',
  'status' => 'Status',
  'active' => 'Active',
  'yes' => 'Yes',
  'no' => 'No',
  'not_active' => 'Not active',
  'product' => 'Product',
  'discount_value' => 'Discount value',
  'expiration_date' => 'Expiration date',
  'information' => 'Information'
);
