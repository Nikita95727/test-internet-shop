@extends('layouts.app')
@section('content')
	<div class="row account-dashboard">
		<div class="col-md-10 col-lg-10 col-xs-12">
			<h3>{{__('Адресная книга')}}</h3><p></p>
            @if (session()->exists('success'))
                <div class="alert alert-success" role="alert">
                    {{session()->get('success')}}
                </div>
            @endif
            @if(!empty(Auth::user()->getUserAddresses()))
                @foreach (Auth::user()->getUserAddresses() as $address)
                    <div class="card dashboard-address-item">
                        <div class="card-body">
                            <a href="#" data-toggle="modal" data-target="#removeShippingAddress{{$address->id}}">
                                <i class="fas fa-times"></i>
                            </a>
                            <div class="modal" tabindex="-1" id="removeShippingAddress{{$address->id}}" aria-labelledby="exampleModalLabel" aria-hidden="true" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">{{__('Вы уверены, что желаете удалить адрес?')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-footer">
                                            <a href="{{URL::to('/dashboard/removeShippingAddress/'.$address->id)}}"><button type="button" class="btn btn-primary">{{__('Удалить адрес')}}</button></a>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Отмена')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <address>
                                <div class="user-address-city">
                                    {{__('Город: '.$address->getUser()->getUserCity())}}
                                </div>
                                <div class="user-address-home-address">
                                    {{__('Адрес: '.$address->address)}}
                                </div>
                                <div class="user-post-index">
                                    {{__('Почтовый индекс: '.$address->getUser()->getUserPostCode())}}
                                </div>
                                <div class="user-telephone">
                                    {{__('Телефон: '.$address->getUser()->getUserTelephone())}}
                                </div>
                            </address>
                        </div>
                    </div>
                @endforeach
            @endif
            <div class="add-shipping-address-block">
                <a href="#" data-toggle="modal" data-target="#addShippingAddressModal">
                    <button type="submit" class="btn btn-info" >{{__('Добавить новый адрес')}}</button>
                </a>
                <div class="modal" tabindex="-1" id="addShippingAddressModal" aria-labelledby="exampleModalLabel" aria-hidden="true" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">{{__('Изменение роли')}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form method="post" action="{{URL::to('/dashboard/addShippingAddress')}}">
                                <div class="modal-body">
                                    @csrf
                                    <input type="hidden" name="userId" value="{{Auth::user()->id}}">
                                    <label for="userCity">{{__('Город')}}</label>
                                    <input type="text" class="form-control" placeholder="{{__('Введите название города')}}" name="userCity">
                                    <label for="userTelephone">{{__('Улица и номер дома')}}</label>
                                    <input type="text" class="form-control" placeholder="{{__('Введите улицу и номер дома')}}" name="userAddress">
                                    <label for="userTelephone">{{__('Номер телефона')}}</label>
                                    <input type="tel" class="form-control" placeholder="{{__('Введите номер телефона')}}" name="userTelephone">
                                    <label for="userPostCode">{{__('Почтовый индекс')}}</label>
                                    <input type="text" class="form-control" placeholder="{{__('Введите почтовый индекс')}}" name="userPost">
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">{{__('Добавить адрес')}}</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Отмена')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
		</div>
        @include('layouts.dashboard.menu')
    </div>
@endsection
