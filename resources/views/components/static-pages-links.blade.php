<div class="buttom-box">
    <div class="quick-inform">
        <p class="text-white">{{ __('text.information') }}</p>
    </div>
    <div class="quick-inform-link">
        @if(!empty($staticPages))
            @foreach($staticPages as $staticPage)
                <a class="href-link" href="{{ URL::to('/staticPage/'.$staticPage->id) }}">{{ $staticPage->title }}</a>
            @endforeach
        @endif
    </div>
</div>
