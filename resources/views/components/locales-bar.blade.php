<div>
    <li class="dropdown indent-right">
        <a class="nav-link dropdown-toggle link-height text-white" href="#"
           id="navbar-language"
           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Language</a>
        <div class="dropdown-menu" aria-labelledby="navbar-language">
            @foreach($locales as $locale)
                <a class="dropdown-item" href="#">{{ $locale->localeCode }}</a>
            @endforeach
        </div>
    </li>
</div>
