<div class="btn-group">
    <div class="menu-cart" data-toggle="dropdown" aria-haspopup="true"
         aria-expanded="false">
        <button class="menu-cart-text no-margin text-white"><i
                class="fas fa-shopping-cart"></i></button>
    </div>
    <div class="dropdown-menu dropdown-menu-right dropdown-cart"
         aria-labelledby="dropdown-cart">
        @if(!empty($cartItems))
            <div class="cart-table-box">
                <table class="table">
                    <tbody>
                    @foreach($cartItems as $id => $cartItem)
                        <tr>
                            <td>
                                <a href="{{URL::to('/shop/show/'.$cartItem['id'])}}">
                                    <img class="cart-img-td"
                                         src="{{$cartItem['image']}}"
                                         alt="Cart image">
                                </a>
                            </td>
                            <td>
                                <a href="{{URL::to('/shop/show/'.$cartItem['id'])}}" class="cart-href">
                                    {{$cartItem['title']}}
                                </a>
                            </td>
                            <td><span
                                    class="total-products-price{{$cartItem['id']}}">{{$cartItem['qty'] * $cartItem['price']}} {{ $storeCurrencySymbol }}</span>
                            </td>
                            <td><a href="#" data-toggle="modal" data-target="#deleteItem{{$cartItem['id']}}"><i
                                        class="fas fa-trash-alt"></i></a></td>
                            <div class="modal fade" id="deleteItem{{$cartItem['id']}}" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title"
                                                id="exampleModalLabel">{{__('Подтверждение удаления')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <h6>{{__('Вы уверены, что хотите удалить данный продукт из корзины?')}}</h6>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">{{__('Закрыть')}}</button>
                                            <a href="{{URL::to('/shop/cart/removeProduct/'.$cartItem['id'])}}">
                                                <button type="button" class="btn btn-primary">Удалить</button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <hr>
            <div class="after-cart-table">
                <p class="no-margin subtotal-right">
                    <b>Subtotal:</b> {{ $cart->getSubtotal() }} {{ $storeCurrencySymbol }}</p>
                <div class="button-cart">
                    <a href="{{ URL::to('/shop/cart') }}" class="btn btn-rectangle-cart btn-danger"><p
                            class="no-margin">VIEW CART</p></a>
                    <a href="{{ URL::to('/shop/checkout/index') }}" class="btn btn-rectangle-cart btn-danger"><p
                            class="no-margin">CHECKOUT</p></a>
                </div>
            </div>
        @else
            <a class="dropdown-item">Ooops, your cart is empty.</a>
        @endif
    </div>
</div>

