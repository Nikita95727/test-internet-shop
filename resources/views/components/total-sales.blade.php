<div>
    <div class="total-sales-qty" id="total-sales-card">
        <div class="row">
            <div class="col-9">
                <div class="total-sales-qty-header">
                    <span>{{ __('admin.total_sales') }}</span>
                    <div>
                        <span class="total-sales-qty-value">{{ $totalSales }} {{ $currencySymbol }}</span>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="total-sales-qty-body">
                    <i class="fas fa-piggy-bank"></i>
                </div>
            </div>
        </div>
        <div class="total-sales-qty-bottom"></div>
    </div>
</div>
