<div class="container-main-body">
    <div class="row row-body">
        <div class="body-header">
            <h2>Recent Arrival</h2>
        </div>
        <div class="new-product">
            @foreach($latestProducts as $latestProduct)
                <div class="card card-box">
                    <div class="card-header">
                        <img class="card-img-top" src="{{ asset('images/mobile-phone.png') }}" alt="Card image cap">
                        @if($latestProduct->getDiscount())
                            <div class="discount-prod">
                                <a href="#" class="btn btn-discount"><p class="text-white no-margin">{{ $latestProduct->getDiscount() }}%</p></a>
                            </div>
                        @endif
                        <div class="button-hover">
                            <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-heart"></i></a>
                            <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-balance-scale"></i></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">{{ $latestProduct->getTitle() }}</h5>
                        <p class="card-text">{{ $latestProduct->getShortDescription() }}</p>
                        <div class="stars">
                            <p>top stars</p>
                        </div>
                        <div class="price">
                            <p>{{ $latestProduct->getPrice() }} {{ $storeCurrencySymbol }}</p>
                        </div>
                        <div class="button-cart">
                            <a href="{{ URL::to('/shop/buy/'.$latestProduct->id) }}" class="btn btn-rectangle-cart btn-danger"><i class="fas fa-shopping-cart"></i><p class="no-margin"> Shop me baby!</p></a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="button-view">
            <button class="btn btn-danger btn-view">View more product</button>
        </div>
    </div>
</div>
