@if (isset($shippingMethods))
    <div class="form-check">
        @foreach ($shippingMethods as $shippingMethod)
            <input class="form-check-input" type="radio" name="shippingMethod" id="shippingMethodId" value="{{ $shippingMethod->id }}">
            <label class="form-check-label" for="shippingMethodId">
                {{$shippingMethod->shippingMethodName}} - {{$shippingMethod->shippingMethodPrice}} {{__('грн')}}
            </label>
        @endforeach
    </div>
@endif
