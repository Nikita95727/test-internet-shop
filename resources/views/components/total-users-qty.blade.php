<div>
    <div class="total-users-qty" id="total-users-card">
        <div class="row">
            <div class="col-9">
                <div class="total-users-qty-header">
                    <span>{{ __('admin.total_users') }}</span>
                    <div>
                        <span class="total-users-qty-value">{{ $qty }}</span>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="total-users-qty-body">
                    <i class="fas fa-user"></i>
                </div>
            </div>
        </div>
        <div class="total-users-qty-bottom"></div>
    </div>
</div>
