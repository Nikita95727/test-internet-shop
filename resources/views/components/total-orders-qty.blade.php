<div>
    <div class="total-orders-qty" id="total-orders-card">
        <div class="row">
            <div class="col-9">
                <div class="total-orders-qty-header">
                    <span>{{ __('admin.total_orders') }}</span>
                    <div>
                        <span class="total-orders-qty-value">{{ $qty }}</span>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="total-orders-qty-body">
                    <i class="fas fa-shopping-cart"></i>
                </div>
            </div>
        </div>
        <div class="total-orders-qty-bottom"></div>
    </div>
</div>
