<div>
    <nav class="navbar navbar-expand mb-4 static-top admin-top-panel">
        <div class="admin-top-panel-icon">
            <i class="fas fa-flag"></i><span><sup>9</sup></span>
        </div>
        <div class="admin-top-panel-icon">
            <i class="far fa-bell"></i><span><sup>9</sup></span>
        </div>
        <div class="col-7">
            <div class="input-group input-group-sm">
                <input type="text" class="form-control" value="search..." required>
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                </div>
            </div>
        </div>
        <span class="sr-only">{{ __('admin.new_orders') }}</span>
        @if($currentLocale)
            <a class="nav-link dropdown-toggle" href="#" id="localeDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"></span>
                <img class="img-profile rounded-circle" src="{{ asset('/storage/'.$currentLocale->localeFlag) }}" width="50px" height="50px">
            </a>
        @endif
        <!-- Dropdown - User Information -->
        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="localeDropdown">
            @if($locales->isNotEmpty())
                @foreach($locales as $locale)
                    <a class="dropdown-item" href="#">
                        {{ $locale->localeName }}
                    </a>
                @endforeach
            @endif
        </div>
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{Auth::user()->name}}</span>
            <img class="img-profile rounded-circle" src="{{asset(Auth::user()->avatar)}}" width="50px" height="50px">
        </a>
        <!-- Dropdown - User Information -->
        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                {{ __('Выйти из аккаунта') }}
            </a>
        </div>
    </nav>
</div>
