<label for="shopLocale">{{ __('admin.localization') }}</label>
<select class="form-control" name="shopLocales[]">
    <option></option>
    @foreach($locales as $locale)
        <option value="{{ $locale->id }}">{{ $locale->localeCode }}</option>
    @endforeach
</select>
