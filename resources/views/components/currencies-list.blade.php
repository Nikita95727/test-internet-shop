<label for="shopCurrency">{{ __('admin.currency') }}</label>
<select class="form-control" name="shopCurrencies[]">
    <option></option>
    @foreach($currencies as $currency)
        <option value="{{ $currency->id }}">{{ $currency->currencyCode }}</option>
    @endforeach
</select>
