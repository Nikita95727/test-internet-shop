<div>
    <li class="dropdown indent-right">
        <a class="nav-link dropdown-toggle link-height text-white" href="#"
           id="navbar-currency"
           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Currency</a>
        <div class="dropdown-menu" aria-labelledby="navbar-currency">
            @foreach($currencies as $currency)
                <a class="dropdown-item" href="#">{{ $currency->currencyCode }} {{ $currency->currencySymbol }}</a>
            @endforeach
        </div>
    </li>
</div>
