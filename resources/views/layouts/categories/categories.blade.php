<div class="categories">
    <div class="card bg-primary">
        @if (count($categories) > 0)
            <div class="card-header bg-primary">{{__('Категории')}}</div>
            <category-component :categories="{{ $categories }}"></category-component>
        @endif
    </div>
</div>
