<div class="col-md-2 col-lg-2 col-xl-2">
    <div class="dashboard-menu">
        <ul>
            <li><a href="{{URL::to('/dashboard')}}">{{__('Личный кабинет')}}</a></li>
            <li><a href="{{URL::to('/dashboard/info')}}">{{__('Моя информация')}}</a></li>
            <li><a href="{{URL::to('/dashboard/address')}}">{{__('Мои адреса')}}</a></li>
            <li><a href="{{URL::to('/dashboard/equal')}}">{{__('Мои сравнения')}}</a></li>
            <li><a href="{{URL::to('/dashboard/wishes')}}">{{__('Список желаний')}}</a></li>
        </ul>
    </div>
</div>
