<div class="col-3 admin-dashboard">
    <ul class="navbar-nav">
        <div class="sidebar-brand-text admin-dashboard-header">
            <a class="nav-link" href="{{ URL::to('/admin') }}">
                <span><i class="fas fa-border-all"></i>{{__('admin.dashboard')}}</span>
            </a>
        </div>
        <hr class="sidebar-divider my-0">
        @if (Auth::user()->hasAccess())
            @if (Auth::user()->isAdmin())
                <li class="nav-item active">
                    <a class="nav-link" href="{{URL::to('/admin/catalog')}}">
                        <span><i class="fas fa-air-freshener"></i>{{__('admin.catalog')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/shop/admin/managecustomers')}}">
                        <span><i class="fas fa-frown-open"></i>{{__('admin.users')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/admin/manageOrders')}}">
                        <span><i class="fas fa-check"></i>{{__('admin.orders')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/shop/admin/infoshippingmethods')}}">
                        <span><i class="fas fa-shipping-fast"></i>{{__('admin.delivery_methods')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/shop/admin/infopaymentmethods')}}">
                        <span><i class="fas fa-money-bill-alt"></i> {{ __('admin.payment_methods') }}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/shop/admin/sales')}}">
                        <span><i class="fas fa-piggy-bank"></i>{{__('admin.sales')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/shop/admin/storeconfigmenu')}}">
                        <span><i class="fas fa-cogs"></i>{{__('admin.store_config')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/admin/manageCache')}}">
                        <span><i class="fas fa-cookie"></i>{{__('admin.cache_management')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL::to('/shop/admin/design') }}"><span><i class="fas fa-palette"></i></span>{{ __('admin.design') }}</a>
                </li>
            @else
                <li class="nav-item active">
                    <a class="nav-link" href="{{URL::to('/admin')}}">
                        <span><i class="fas fa-air-freshener"></i>{{__('Продукты')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/shop/admin/managecustomers')}}">
                        <span><i class="fas fa-grin-beam-sweat"></i><i class="fas fa-frown-open"></i>{{__('Пользователи')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/admin/manageOrders')}}">
                        <span><i class="fas fa-check"></i>{{__('Заказы')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/shop/admin/infoshippingmethods')}}">
                        <span><i class="fas fa-shipping-fast"></i>{{__('Способы доставки')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/shop/admin/infopaymentmethods')}}">{{ __('Способы оплаты') }}</a>
                </li>
            @endif
        @endif
    </ul>
</div>
