<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://kit.fontawesome.com/ae7e200d98.js"></script>
    <script type="text/javascript" src="js/modernizr.custom.28468.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mainPage.css') }}" rel="stylesheet">
    <link href="{{ asset('css/checkout/cart.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <div class="top-header"> {{--maybe mobile menu--}}
        <div class="first-menu">
            <nav class="navbar navbar-expand-md navbar-light nav-bg-color no-padding">
                <div class="container big-container">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false"
                            aria-label="{{ __('Toggle navigation') }}">
                        <span><i class="fas fa-bars text-white"></i></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">
                            <x-currencies-bar/>
                            <x-locales-bar/>
                            @if(Auth::check())
                                <li class="indent-right">
                                    <a class="nav-link link-height text-white" href="#"><i class="fas fa-heart"></i>My
                                        favorites (0)</a>
                                </li>
                            @endif
                            <li class="indent-right">
                                <a class="nav-link link-height text-white" href="#"><i class="fas fa-balance-scale"></i>Compare
                                    products (0)</a>
                            </li>
                            <li class="indent-right">
                                <a class="nav-link link-height text-white" href="#"><i
                                        class="fas fa-car"></i>Delivery</a>
                            </li>
                        </ul>
                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                                <li class="nav-item">
                                    <a class="nav-link text-white" href="{{ route('login') }}"><i
                                            class="fas fa-key"></i> {{ __('Login') }}</a>
                                </li>
                                <div class="vl"></div>
                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link text-white"
                                           href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <div id="fixed" class="second-menu absolute-menu">
            <div class="container big-container">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <a class="navbar-brand text-dark" href="{{ url('/') }}">
                            <img class="logo-style"
                                 src="{{ asset('images/Logo-Test.png') }}">{{--{{ config('app.name', 'Laravel') }}--}}
                        </a>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                        <div class="top-nav-secondmenu">
                            <div class="navbar navbar-expand-lg nav-height navbar-light">
                                <div class="collapse nav-height navbar-collapse">
                                    <ul class="nav navbar-nav navbar-nav-height">
                                        <x-mega-menu/>
                                        <li class="indent-right">
                                            <a class="nav-link" href="#">News</a>
                                        </li>
                                        <li class="indent-right">
                                            <a class="nav-link" href="#">FAQ</a>
                                        </li>
                                        <li class="indent-right">
                                            <a class="nav-link" href="#">Information</a>
                                        </li>
                                        <li class="indent-right">
                                            <a class="nav-link" href="#">Creators</a>
                                        </li>
                                    </ul>
                                    <div class="input-group col-md-5">
                                        <input type="text" class="form-control text-white" placeholder="Search"
                                               aria-label="Search">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-dark text-white" type="submit"><i
                                                    class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                    <div class="btn-group">
                                        <div class="phone-number" data-toggle="dropdown"
                                             aria-haspopup="true" aria-expanded="false">
                                            <button class="menu-phone-text no-margin text-white"><i
                                                    class="fas fa-phone"></i></button>
                                        </div>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-phone">
                                            <a class="dropdown-item"></a>
                                        </div>
                                    </div>
                                    <x-mini-cart/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <main class="content">
        @yield('content')
    </main>
    <footer class="site-footer">
        <div class="first-footer">
            <div class="container big-container">
                <div class="footer-box">
                    <div class="buttom-box">
                        <div class="footer-img">
                            <a class="href-logo" href="#"><img class="logo-img" src="{{ 'images/Logo-Test.png' }}"
                                                               alt="logo"></a>
                        </div>
                        <div class="location-info">
                            <p class="no-margin text-white">60, 29th Street, San Francisco, CA 94110, United States
                                of America</p>
                        </div>
                        <div class="email-info">
                            <a class="email-link" href="#">zzz@zzz.zzz</a>
                        </div>
                        <div class="phone-info">
                            <p class="text-white">+38(096)012-34-56</p>
                        </div>
                    </div>
                    <x-static-pages-links/>
                    <div class="buttom-box">
                        <div class="quick-inform">
                            <p class="text-white">Newsletter</p>
                        </div>
                        <div class="send-list">
                            <p class="text-white">Sign Up For Latest Updates For News</p>
                            <div class="input-group input-list-footer mb-1">
                                <input type="text" class="form-control">
                            </div>
                            <button class="btn btn-danger footer-btn" type="submit">Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr class="color-line no-margin">
        <div class="second-footer">
            <div class="container big-container">
                <div class="row">
                    <div class="footer-box">
                        <div class="caption">
                            <p class="no-margin text-white">Copyright © 2020 Templatemela, All Rights Reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script>
        window.onscroll = function () {
            myFunction()
        };

        var navbar = document.getElementById("fixed");
        var sticky = navbar.offsetTop;

        function myFunction() {
            if (window.pageYOffset >= sticky) {
                navbar.classList.add("fixed-menu")
            } else {
                navbar.classList.remove("fixed-menu");
            }
        }
    </script>
</div>
</body>
</html>
