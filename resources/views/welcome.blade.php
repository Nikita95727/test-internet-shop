@extends('layouts.app')
@section('content')
    <div id="sliderIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#sliderIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#sliderIndicators" data-slide-to="1"></li>
            <li data-target="#sliderIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="body-slide">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <img class="img-size" src="{{ 'images/mobile-phone.png' }}">
                            </div>
                            <div class="col-md-6 text-white center-block-slider">
                                <div class="sl-text ">
                                    <h2 class="text-font">Some smartphone ebobo</h2>
                                    <hr align="left" width="260" size="4" color="#ff9900"/>
                                    <h3><b>By this stupid phone you must!</b></h3>
                                    <a class="btn btn-danger btn-slide">Shop now!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="body-slide">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <img class="img-size" src="{{ 'images/samsung-galaxy-note-7.png' }}">
                            </div>
                            <div class="col-md-6 text-white center-block-slider">
                                <div class="sl-text ">
                                    <h2 class="text-font">Some smartphone ebobo</h2>
                                    <hr align="left" width="260" size="4" color="#ff9900"/>
                                    <h3><b>By this stupid phone you must!</b></h3>
                                    <a class="btn btn-danger btn-slide">Shop now!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="body-slide">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <img class="img-size" src="{{ 'images/feature-phone.png' }}">
                            </div>
                            <div class="col-md-6 text-white center-block-slider">
                                <div class="sl-text ">
                                    <h2 class="text-font">Some smartphone ebobo</h2>
                                    <hr align="left" width="260" size="4" color="#ff9900"/>
                                    <h3><b>By this stupid phone you must!</b></h3>
                                    <a class="btn btn-danger btn-slide">Shop now!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#sliderIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#sliderIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="best-category">
        <div class="row-best-category">
            <div class="col-md-6 col-pad-1 col-opacity">
                <div class="body-category">
                    <div class="col-md-6 center-block">
                        <h2 class="text-font">Some smartphone ebobo</h2>
                        <h3><i><b>Ya tebya tryba shatal</b></i></h3>
                        <a class="btn btn-danger btn-best-category text-white">Shop now!</a>
                    </div>
                    <div class="col-md-6">
                        <img class="img-size" src="{{ 'images/mobile-phone.png' }}">
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-pad-2">
                <div class="body-category">
                    <div class="col-md-6 center-block">
                        <h2 class="text-font">Some smartphone ebobo</h2>
                        <h3><i><b>Ya tebya tryba shatal</b></i></h3>
                        <a class="btn btn-danger btn-best-category text-white">Shop now!</a>
                    </div>
                    <div class="col-md-6">
                        <img class="img-size" src="{{ 'images/mobile-phone.png' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-resent-arrival/>
    <div class="user-service">
        <div class="container">
            <div class="row row-service">
                <div class="user-service-block">
                    <span><i class="fas fas-s fa-award"></i></span>
                    <div class="user-service-text">
                        <a href="#" class="text-white">
                            <h3>Daroy banditki!</h3>
                            <p class="user-service-margin">Chavo kavo dyadya?!</p>
                        </a>
                    </div>
                </div>
                <div class="vl"></div>
                <div class="user-service-block">
                    <span><i class="fas fas-s fa-piggy-bank"></i></span>
                    <div class="user-service-text">
                        <a href="#" class="text-white">
                            <h3>Daroy banditki!</h3>
                            <p class="user-service-margin">Chavo kavo dyadya?!</p>
                        </a>
                    </div>
                </div>
                <div class="vl"></div>
                <div class="user-service-block">
                    <span><i class="fas fas-s fa-lock"></i></span>
                    <div class="user-service-text">
                        <a href="#" class="text-white">
                            <h3>Daroy banditki!</h3>
                            <p class="user-service-margin">Chavo kavo dyadya?!</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="best-deals">
        <div class="container container-best-deals">
            <div class="row row-deals">
                <div class="deals-all">
                    <div class="text-deals text-white">
                        <h2>Weekly Deals</h2>
                        <p class="card-text">Lorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod tempor quis nostrud exercitation
                            ullamco...</p>
                        <a href="#" class="btn btn-danger btn-deals">Shop now!</a>
                        <h3>Hurry! Offer Ends In</h3>
                        <div class="timer">
                            <div class="time-box">
                                <strong>00</strong>days</div>
                            <div class="time-box"><strong>00</strong>hours</div>
                            <div class="time-box"><strong>00</strong>mins</div>
                            <div class="time-box"><strong>00</strong>secs</div>
                        </div>
                    </div>
                    <div class="deals-slider">
                        <div id="carousel-best-deals" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div class="card card-box">
                                        <div class="card-header">
                                            <img class="card-img-top" src="{{ asset('images/mobile-phone.png') }}" alt="Card image cap">
                                            <div class="discount-prod">
                                                <a href="#" class="btn btn-discount"><p class="text-white no-margin">25%</p></a>
                                            </div>
                                            <div class="button-hover">
                                                <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-heart"></i></a>
                                                <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-balance-scale"></i></a>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-title">Some title</h5>
                                            <hr>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                                the card's content.</p>
                                            <div class="stars">
                                                <p>top stars</p>
                                            </div>
                                            <div class="price">
                                                <p class="price-marg">$128</p>
                                            </div>
                                            <div class="button-cart">
                                                <a href="#" class="btn btn-rectangle-cart btn-danger"><i class="fas fa-shopping-cart"></i><p class="no-margin"> Shop me baby!</p></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="card card-box">
                                        <div class="card-header">
                                            <img class="card-img-top" src="{{ asset('images/mobile-phone.png') }}" alt="Card image cap">
                                            <div class="discount-prod">
                                                <a href="#" class="btn btn-discount"><p class="text-white no-margin">30%</p></a>
                                            </div>
                                            <div class="button-hover">
                                                <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-heart"></i></a>
                                                <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-balance-scale"></i></a>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-title">Some title</h5>
                                            <hr>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                                the card's content.</p>
                                            <div class="stars">
                                                <p>top stars</p>
                                            </div>
                                            <div class="price">
                                                <p class="price-marg">$128</p>
                                            </div>
                                            <div class="button-cart">
                                                <a href="#" class="btn btn-rectangle-cart btn-danger"><i class="fas fa-shopping-cart"></i><p class="no-margin"> Shop me baby!</p></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="card card-box">
                                        <div class="card-header">
                                            <img class="card-img-top" src="{{ asset('images/mobile-phone.png') }}" alt="Card image cap">
                                            <div class="discount-prod">
                                                <a href="#" class="btn btn-discount"><p class="text-white no-margin">20%</p></a>
                                            </div>
                                            <div class="button-hover">
                                                <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-heart"></i></a>
                                                <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-balance-scale"></i></a>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-title">Some title</h5>
                                            <hr>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                                the card's content.</p>
                                            <div class="stars">
                                                <p>top stars</p>
                                            </div>
                                            <div class="price">
                                                <p class="price-marg">$128</p>
                                            </div>
                                            <div class="button-cart">
                                                <a href="#" class="btn btn-rectangle-cart btn-danger"><i class="fas fa-shopping-cart"></i><p class="no-margin"> Shop me baby!</p></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carousel-best-deals" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel-best-deals" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-main-body">
        <div class="row row-body">
            <div class="body-header">
                <h2>Our Popular Products</h2>
            </div>
            <div class="new-product">
                <div class="card card-box">
                    <div class="card-header">
                        <img class="card-img-top" src="{{ asset('images/mobile-phone.png') }}" alt="Card image cap">
                        <div class="discount-prod">
                            <a href="#" class="btn btn-discount"><p class="text-white no-margin">25%</p></a>
                        </div>
                        <div class="button-hover">
                            <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-heart"></i></a>
                            <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-balance-scale"></i></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Smart</h5>
                        <p class="card-text">some text</p>
                        <div class="stars">
                            <p>top stars</p>
                        </div>
                        <div class="price">
                            <p>$128</p>
                        </div>
                        <div class="button-cart">
                            <a href="#" class="btn btn-rectangle-cart btn-danger"><i class="fas fa-shopping-cart"></i><p class="no-margin"> Shop me baby!</p></a>
                        </div>
                    </div>
                </div>
                <div class="card card-box">
                    <div class="card-header">
                        <img class="card-img-top" src="{{ asset('images/mobile-phone.png') }}" alt="Card image cap">
                        <div class="button-hover">
                            <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-heart"></i></a>
                            <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-balance-scale"></i></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Smart</h5>
                        <p class="card-text">some text</p>
                        <div class="stars">
                            <p>top stars</p>
                        </div>
                        <div class="price">
                            <p>$128</p>
                        </div>
                        <div class="button-cart">
                            <a href="#" class="btn btn-rectangle-cart btn-danger"><i class="fas fa-shopping-cart"></i><p class="no-margin"> Shop me baby!</p></a>
                        </div>
                    </div>
                </div>
                <div class="card card-box">
                    <div class="card-header">
                        <img class="card-img-top" src="{{ asset('images/mobile-phone.png') }}" alt="Card image cap">
                        <div class="button-hover">
                            <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-heart"></i></a>
                            <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-balance-scale"></i></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Smart</h5>
                        <p class="card-text">some text</p>
                        <div class="stars">
                            <p>top stars</p>
                        </div>
                        <div class="price">
                            <p>$128</p>
                        </div>
                        <div class="button-cart">
                            <a href="#" class="btn btn-rectangle-cart btn-danger"><i class="fas fa-shopping-cart"></i><p class="no-margin"> Shop me baby!</p></a>
                        </div>
                    </div>
                </div>
                <div class="card card-box">
                    <div class="card-header">
                        <img class="card-img-top" src="{{ asset('images/mobile-phone.png') }}" alt="Card image cap">
                        <div class="button-hover">
                            <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-heart"></i></a>
                            <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-balance-scale"></i></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Smart</h5>
                        <p class="card-text">some text</p>
                        <div class="stars">
                            <p>top stars</p>
                        </div>
                        <div class="price">
                            <p>$128</p>
                        </div>
                        <div class="button-cart">
                            <a href="#" class="btn btn-rectangle-cart btn-danger"><i class="fas fa-shopping-cart"></i><p class="no-margin"> Shop me baby!</p></a>
                        </div>
                    </div>
                </div>
                <div class="card card-box">
                    <div class="card-header">
                        <img class="card-img-top" src="{{ asset('images/mobile-phone.png') }}" alt="Card image cap">
                        <div class="button-hover">
                            <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-heart"></i></a>
                            <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-balance-scale"></i></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Smart</h5>
                        <p class="card-text">some text</p>
                        <div class="stars">
                            <p>top stars</p>
                        </div>
                        <div class="price">
                            <p>$128</p>
                        </div>
                        <div class="button-cart">
                            <a href="#" class="btn btn-rectangle-cart btn-danger"><i class="fas fa-shopping-cart"></i><p class="no-margin"> Shop me baby!</p></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="button-view">
                <button class="btn btn-danger btn-view">View more product</button>
            </div>
        </div>
    </div>
    <div class="best-deals-second">
        <div class="container container-best-deals">
            <div class="row row-deals">
                <div class="deals-all">
                    <div class="text-deals text-white">
                        <h2>Explore Smart</h2>
                        <h3 class="card-text">Camera Accessories</h3>
                        <a href="#" class="btn btn-danger btn-deals">Shop now!</a>
                    </div>
                    <div class="deals-slider">
                        <div id="carousel-deals-second" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div class="card card-box">
                                        <div class="card-header">
                                            <img class="card-img-top" src="{{ asset('images/mobile-phone.png') }}" alt="Card image cap">
                                            <div class="discount-prod">
                                                <a href="#" class="btn btn-discount"><p class="text-white no-margin">25%</p></a>
                                            </div>
                                            <div class="button-hover">
                                                <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-heart"></i></a>
                                                <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-balance-scale"></i></a>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-title">Some title</h5>
                                            <hr>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                                the card's content.</p>
                                            <div class="stars">
                                                <p>top stars</p>
                                            </div>
                                            <div class="price">
                                                <p class="price-marg">$128</p>
                                            </div>
                                            <div class="button-cart">
                                                <a href="#" class="btn btn-rectangle-cart btn-danger"><i class="fas fa-shopping-cart"></i><p class="no-margin"> Shop me baby!</p></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="card card-box">
                                        <div class="card-header">
                                            <img class="card-img-top" src="{{ asset('images/mobile-phone.png') }}" alt="Card image cap">
                                            <div class="discount-prod">
                                                <a href="#" class="btn btn-discount"><p class="text-white no-margin">30%</p></a>
                                            </div>
                                            <div class="button-hover">
                                                <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-heart"></i></a>
                                                <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-balance-scale"></i></a>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-title">Some title</h5>
                                            <hr>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                                the card's content.</p>
                                            <div class="stars">
                                                <p>top stars</p>
                                            </div>
                                            <div class="price">
                                                <p class="price-marg">$128</p>
                                            </div>
                                            <div class="button-cart">
                                                <a href="#" class="btn btn-rectangle-cart btn-danger"><i class="fas fa-shopping-cart"></i><p class="no-margin"> Shop me baby!</p></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="card card-box">
                                        <div class="card-header">
                                            <img class="card-img-top" src="{{ asset('images/mobile-phone.png') }}" alt="Card image cap">
                                            <div class="discount-prod">
                                                <a href="#" class="btn btn-discount"><p class="text-white no-margin">20%</p></a>
                                            </div>
                                            <div class="button-hover">
                                                <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-heart"></i></a>
                                                <a href="#" class="btn btn-circle btn-danger"><i class="fas fa-balance-scale"></i></a>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-title">Some title</h5>
                                            <hr>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                                the card's content.</p>
                                            <div class="stars">
                                                <p>top stars</p>
                                            </div>
                                            <div class="price">
                                                <p class="price-marg">$128</p>
                                            </div>
                                            <div class="button-cart">
                                                <a href="#" class="btn btn-rectangle-cart btn-danger"><i class="fas fa-shopping-cart"></i><p class="no-margin"> Shop me baby!</p></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carousel-deals-second" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel-deals-second" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-main-body">
        <div class="row row-body">
            <div class="body-header">
                <h2>From The Blog</h2>
            </div>
            <div class="blog-news">
                <div id="carousel-news" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="card card-box">
                                        <div class="card-header">
                                            <img class="card-img-top" src="{{ 'images/mobile-phone.png' }}" alt="Card image cap">
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-title">Lol</h5>
                                            <hr>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                                the card's content.</p>
                                            <div class="footer-block-card">
                                                <a href="#" class="footer-block-href"><b>Read more...</b></a>
                                                <p>10.12.2020</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card card-box">
                                        <div class="card-header">
                                            <img class="card-img-top" src="{{ 'images/mobile-phone.png' }}" alt="Card image cap">
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <hr>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                                the card's content.</p>
                                            <div class="footer-block-card">
                                                <a href="#" class="footer-block-href"><b>Read more...</b></a>
                                                <p>10.12.2020</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card card-box">
                                        <div class="card-header">
                                            <img class="card-img-top" src="{{ 'images/mobile-phone.png' }}" alt="Card image cap">
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <hr>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                                the card's content. Some quick example text to build on the card title and make up the bulk of
                                                the card's content.</p>
                                            <div class="footer-block-card">
                                                <a href="#" class="footer-block-href"><b>Read more...</b></a>
                                                <p>10.12.2020</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="card card-box">
                                        <div class="card-header">
                                            <img class="card-img-top" src="{{ 'images/mobile-phone.png' }}" alt="Card image cap">
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-title">Kek</h5>
                                            <hr>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                                the card's content.</p>
                                            <div class="footer-block-card">
                                                <a href="#" class="footer-block-href"><b>Read more...</b></a>
                                                <p>10.12.2020</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card card-box">
                                        <div class="card-header">
                                            <img class="card-img-top" src="{{ 'images/mobile-phone.png' }}" alt="Card image cap">
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <hr>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                                the card's content.</p>
                                            <div class="footer-block-card">
                                                <a href="#" class="footer-block-href"><b>Read more...</b></a>
                                                <p>10.12.2020</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card card-box">
                                        <div class="card-header">
                                            <img class="card-img-top" src="{{ 'images/mobile-phone.png' }}" alt="Card image cap">
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <hr>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                                the card's content. Some quick example text to build on the card title and make up the bulk of
                                                the card's content.</p>
                                            <div class="footer-block-card">
                                                <a href="#" class="footer-block-href"><b>Read more...</b></a>
                                                <p>10.12.2020</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="card card-box">
                                        <div class="card-header">
                                            <img class="card-img-top" src="{{ 'images/mobile-phone.png' }}" alt="Card image cap">
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-title">Haha</h5>
                                            <hr>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                                the card's content.</p>
                                            <div class="footer-block-card">
                                                <a href="#" class="footer-block-href"><b>Read more...</b></a>
                                                <p>10.12.2020</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card card-box">
                                        <div class="card-header">
                                            <img class="card-img-top" src="{{ 'images/mobile-phone.png' }}" alt="Card image cap">
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <hr>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                                the card's content.</p>
                                            <div class="footer-block-card">
                                                <a href="#" class="footer-block-href"><b>Read more...</b></a>
                                                <p>10.12.2020</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card card-box">
                                        <div class="card-header">
                                            <img class="card-img-top" src="{{ 'images/mobile-phone.png' }}" alt="Card image cap">
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <hr>
                                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                                the card's content. Some quick example text to build on the card title and make up the bulk of
                                                the card's content.</p>
                                            <div class="footer-block-card">
                                                <a href="#" class="footer-block-href"><b>Read more...</b></a>
                                                <p>10.12.2020</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carousel-news" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel-news" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
