@extends('layouts.app')
@section('content')
    <div class="container cart-container">
        <h1 class="cart-marg-h1">Корзина</h1>
        @if (session()->exists('success'))
            <div class="alert alert-success" role="alert">
                {{session()->get('success')}}
            </div>
        @endif
        @if (session()->exists('cart'))
            <table class="table table-hover product-table-cart">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">{{__('Продукт')}}</th>
                    <th scope="col"></th>
                    <th scope="col">{{__('Количество')}}</th>
                    <th scope="col">{{__('Стоимость')}}</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach (session('cart') as $id => $cartItems)
                    <tr>
                        <td class="center-element">
                            <a href="{{URL::to('/shop/show/'.$cartItems['id'])}}">
                                <img src="{{$cartItems['image']}}" height="64px" width="64px">
                            </a>
                        </td>
                        <td>
                            <a href="{{URL::to('/shop/show/'.$cartItems['id'])}}">
                                {{$cartItems['title']}}
                            </a>
                        </td>
                        <td class="center-element">
                            <div class="input-group">
                                <button class="btn btn-danger btn-add" onclick="addQty({{ $cartItems['id'] }})">
                                    <i class="add-qty-btn fas fa-plus-square"></i>
                                </button>
                                <input type="text" class="qty-input-value line-input" onchange="changeQty({{ $cartItems['id'] }})"
                                       id="qty-input-value{{ $cartItems['id'] }}" value="{{$cartItems['qty']}}"
                                       name="qty-input">
                                <button class="btn btn-danger btn-minus" onclick="minusQty({{ $cartItems['id'] }})">
                                    <i class="minus-qty-btn fas fa-minus-square"></i>
                                </button>
                            </div>
                        </td>
                        <td class="center-element"><span
                                class="total-products-price{{$cartItems['id']}}">{{$cartItems['qty'] * $cartItems['price']}}</span>
                            грн
                        </td>
                        <td class="center-element"><a href="#" data-toggle="modal" data-target="#deleteItem{{$cartItems['id']}}"><i
                                    class="fas fa-trash-alt"></i></a></td>
                        <div class="modal fade" id="deleteItem{{$cartItems['id']}}" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title"
                                            id="exampleModalLabel">{{__('Подтверждение удаления')}}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <h6>{{__('Вы уверены, что хотите удалить данный продукт из корзины?')}}</h6>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">{{__('Закрыть')}}</button>
                                        <a href="{{URL::to('/shop/cart/removeProduct/'.$cartItems['id'])}}">
                                            <button type="button" class="btn btn-primary">Удалить</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <a href="{{URL::to('/shop/cart/clearAll')}}" class="btn btn-danger clear-cart-btn">{{__('Очистить корзину')}}</a>
            <div class="container cart-container-body">
                <div class="row">
                    <div class="col-3 special-cards">
                        <div class="form-group">
                            @if(session()->exists('error')): ?>
                            <div class="alert alert-error" role="alert">
                                {{session()->get('error')}}
                            </div>
                            @endif
                            <a href="#" data-toggle="modal" data-target="#addDiscount" class="btn add-discount-btn">
                                <i class="fas fa-credit-card"></i> {{__('Дисконт')}}
                            </a>
                            <div class="modal fade" id="addDiscount" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title"
                                                id="exampleModalLabel">{{__('Добавить дисконтную карту')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form>
                                            {{csrf_field()}}
                                            <div class="modal-body">
                                                <input type="text" class="form-control" name="verifydiscountcard"
                                                       placeholder="{{__('Введите код')}}">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Закрыть')}}</button>
                                                <button type="submit" class="btn btn-primary">{{__('Добавить')}}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <a href="#" data-toggle="modal" data-target="#addGiftCard" class="btn add-gift-card-btn">
                                <i class="fas fa-credit-card"></i> {{__('Подарочная карта')}}
                            </a>
                            <div class="modal fade" id="addGiftCard" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">{{__('Добавить подарочную карту')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="post" action="{{URL::to('/shop/verifygift')}}">
                                            {{csrf_field()}}
                                            <div class="modal-body">
                                                <input type="text" class="form-control" name="verifygiftcard"
                                                       placeholder="{{__('Введите код')}}">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Закрыть')}}</button>
                                                <button type="submit" class="btn btn-primary">{{__('Добавить')}}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <a href="{{URL::to('/shop')}}" class="back-shopping-btn btn btn-dark">
                                {{__('Продолжить покупки')}}
                            </a>
                        </div>
                    </div>
                    <div class="col-6 shipping-methods">
                        <div class="form-group">
                            <label for="shipping-method-input">{{__('Выберите метод доставки')}}</label>
                            <x-delivery-methods/>
                        </div>
                    </div>
                    <div class="col-3 tax">
                        <h3>CART TOTALS</h3>
                        <table class="table table-bordered table-ordering">
                            <tbody>
                                <tr>
                                    <th class="head-light">{{__('Без бонусов:')}}</th>
                                    <td>
                                        @if (isset($cart))
                                            <span class="check-total">{{$cart->getTotal()}}</span>
                                        @endif {{__('грн')}}
                                    </td>
                                </tr>
                                <tr>
                                    <th class="head-light">{{__('ПДВ:')}}</th>
                                    <td>0 грн</td>
                                </tr>
                                <tr>
                                    <th class="head-light">{{__('За доставку:')}}</th>
                                    <td>
                                        <span class="check-shipping-method">
                                            {{ $cart->getShippingMethod() }}
                                        </span> {{__('грн')}}
                                    </td>
                                </tr>
                                <tr>
                                    <th class="head-light">{{__('Дисконт:')}}</th>
                                    <td>
                                        <span class="check-discount">0</span> {{__('грн')}}
                                    </td>
                                </tr>
                                <tr>
                                    @if ($cart->getCupon())
                                    <th class="head-light">{{__('Купон:')}}</th>
                                    <td>
                                        <span class="check-cupon">{{$cart->getCupon()}}</span> {{__('грн')}}
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    @if ($cart->getGiftCard())
                                    <th class="head-light">{{__('Подарочная карта:')}}</th>
                                    <td>
                                        <span class="check-subtotal-span">
                                            <span class="check-gift-cart">-{{$cart->getGiftCard()}} {{__('грн')}}</span>
                                            <a href="{{URL::to('/shop/removegiftcard')}}"
                                               class="remove-gift-card-button">{{__('Отменить')}}</a>
                                        </span>
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <th class="head-light">{{__('Итоговая стоимость:')}}</th>
                                    <td>
                                        <span class="check-total-subtotal">{{ $cart->getSubtotal() }}</span> {{__('грн')}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="checkout-link">
                            <a href="{{URL::to('/shop/checkout/index')}}" class="checkout-btn btn btn-success">
                                {{__('Заказать')}}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if (!session()->exists('cart'))
            <div class="empty-cart">
                <img src="{{asset('img/empty-cart.png')}}">
                <h5>{{__('Корзина пуста')}}</h5>
                <span>{{__('Похоже, что вы не добавили ещё ни одного товара в корзину.')}}</span>
                <div class="empty-cart-continue-shop-link">
                    <a href="{{URL::to('/shop')}}">
                        <button type="button" class="btn btn-primary empty-cart-btn">{{__('Продолжить покупки')}}</button>
                    </a>
                </div>
            </div>
        @endif
    </div>
    <script>
        function setTotalForProduct(value) {
            $('.total-products-price' + value).empty();
            $('.total-products-price' + value).append($('#qty-input-value' + value).val() * {{ isset($cartItems['price']) ? $cartItems['price'] : 0 }});
        }

        function changeQty(value) {
            $.ajax({
                url: '/shop/cart/changeQty/' + value + ',' + $('#qty-input-value' + value).val(),
                type: 'get',
                dataType: 'json',
                success: function (response) {
                    $('#qty-input-value' + value).val(response);
                    setTotalForProduct(value);
                    updateTotal();
                    updateSubtotal();
                }
            })
        }

        function addQty(value) {
            $.ajax({
                url: '/shop/cart/addqty/' + value,
                type: 'get',
                dataType: 'json',
                success: function (response) {
                    $('#qty-input-value' + value).val(response);
                    setTotalForProduct(value);
                    updateTotal();
                    updateSubtotal();
                }
            })
        }

        function minusQty(value) {
            $.ajax({
                url: '/shop/cart/minusqty/' + value,
                type: 'get',
                dataType: 'json',
                success: function (response) {
                    $('#qty-input-value' + value).val(response);
                    setTotalForProduct(value);
                    updateTotal();
                    updateSubtotal();
                }
            })
        }

        function updateTotal() {
            $.ajax({
                url: '/shop/cart/getTotal',
                type: 'get',
                dataType: 'json',
                success: function (response) {
                    $('.check-total').empty();
                    $('.check-total').append(response);
                }
            });
        }

        function updateSubtotal() {
            $.ajax({
                url: '/shop/cart/getSubtotal',
                type: 'get',
                dataType: 'json',
                success: function (response) {
                    $('.check-total-subtotal').empty();
                    $('.check-total-subtotal').append(response);
                }
            });
        }

        $(":radio[name='shippingMethod']").change(function () {
            var radioValue = $(":radio[name='shippingMethod']:checked").val();
            $.ajax({
                url: '/shop/cart/addShippingMethod/' + radioValue,
                type: 'get',
                dataType: 'json',
                success: function (response) {
                    updateSubtotal();
                    $('.check-shipping-method').empty();
                    $('.check-shipping-method').append(response);
                }
            });
        });
    </script>
@endsection
