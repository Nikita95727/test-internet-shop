@extends('layouts.app')
@section('content')
    <div class="container checkout-container">
        <div class="checkout-body">
            <div class="col-8">
            <div class="main-check-info">
                <div class="col-12">
                    <div class="check-info">
                        @guest
                            <div class="users-login">
                                <span class="span-text-right">
                                    <a href="{{ route('login') }}">{{__('Вы зарегистрированны?')}}</a>
                                </span>
                            </div>
                        @endguest
                        <div class="main-card-style">
                            <div class="card card-style">
                                <div class="check-text">
                                    <h3>{{__('Customer')}}</h3>
                                </div>
                                <div class="card-body">
                                    <label for="firstName">First name</label>
                                    <input type="text" class="form-control">
                                    <label for="secondName">Second name</label>
                                    <input type="text" class="form-control">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control">
                                    <label for="password">Password</label>
                                    <input type="text" class="form-control">
                                    <label for="phoneNumber">Phone number</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="card card-style">
                                <div class="check-text">
                                    <h3>{{__('Region')}}</h3>
                                </div>
                                <div class="card-body">
                                    <label for="region">Example select</label>
                                    <select class="form-control" id="region">
                                        <option>1</option>
                                        <option>2</option>
                                    </select>
                                    <label for="city">Example select</label>
                                    <select class="form-control" id="city">
                                        <option>17</option>
                                        <option>21</option>
                                    </select>
                                    <label for="department">Example select</label>
                                    <select class="form-control" id="department">
                                        <option>№21</option>
                                        <option>№2</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="product-checkout">
                        @if (session()->exists('cart'))
                            <table class="table table-hover product-table-cart">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">{{__('Продукт')}}</th>
                                    <th scope="col"></th>
                                    <th scope="col">{{__('Количество')}}</th>
                                    <th scope="col">{{__('Стоимость')}}</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach (session('cart') as $id => $cartItems)
                                    <tr>
                                        <td class="center-element">
                                            <a href="{{URL::to('/shop/show/'.$cartItems['id'])}}">
                                                <img src="{{$cartItems['image']}}" height="64px" width="64px">
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{URL::to('/shop/show/'.$cartItems['id'])}}">
                                                {{$cartItems['title']}}
                                            </a>
                                        </td>
                                        <td class="center-element">
                                            <div class="input-group">
                                                <button class="btn btn-danger btn-add"
                                                        onclick="addQty({{ $cartItems['id'] }})">
                                                    <i class="add-qty-btn fas fa-plus-square"></i>
                                                </button>
                                                <input type="text" class="qty-input-value line-input"
                                                       onchange="changeQty({{ $cartItems['id'] }})"
                                                       id="qty-input-value{{ $cartItems['id'] }}"
                                                       value="{{$cartItems['qty']}}"
                                                       name="qty-input">
                                                <button class="btn btn-danger btn-minus"
                                                        onclick="minusQty({{ $cartItems['id'] }})">
                                                    <i class="minus-qty-btn fas fa-minus-square"></i>
                                                </button>
                                            </div>
                                        </td>
                                        <td class="center-element"><span
                                                class="total-products-price{{$cartItems['id']}}">{{$cartItems['qty'] * $cartItems['price']}}</span>
                                            грн
                                        </td>
                                        <td class="center-element"><a href="#" data-toggle="modal"
                                                                      data-target="#deleteItem{{$cartItems['id']}}"><i
                                                    class="fas fa-trash-alt"></i></a></td>
                                        <div class="modal fade" id="deleteItem{{$cartItems['id']}}" tabindex="-1"
                                             role="dialog"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title"
                                                            id="exampleModalLabel">{{__('Подтверждение удаления')}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h6>{{__('Вы уверены, что хотите удалить данный продукт из корзины?')}}</h6>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">{{__('Закрыть')}}</button>
                                                        <a href="{{URL::to('/shop/cart/removeProduct/'.$cartItems['id'])}}">
                                                            <button type="button" class="btn btn-primary">Удалить
                                                            </button>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
            </div>
            <div class="col-4">
            <div class="second-check-product">
                <div class="col-12">
                    <div class="check-pay">
                        <div class="card card-price">
                            <div class="check-text">
                                <h3>{{__('Delivery method')}}</h3>
                            </div>
                            <div class="card-body">
                                <x-delivery-methods/>
                            </div>
                        </div>
                        <div class="card card-price">
                            <div class="check-text">
                                <h3>{{__('Payment method')}}</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="exampleRadios"
                                           id="exampleRadios1" value="option1">
                                    <label class="form-check-label" for="exampleRadios1">
                                        Default radio
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="checkout-order">
                            <div class="check-text">
                                <h3>{{__('Checkout totals')}}</h3>
                            </div>
                            <table class="table table-bordered table-ordering">
                                <tbody>
                                <tr>
                                    <th class="head-light">{{__('Без бонусов:')}}</th>
                                    <td>
                                        @if (isset($cart))
                                            <span class="check-total">{{$cart->getTotal()}}</span>
                                        @endif {{__('грн')}}
                                    </td>
                                </tr>
                                <tr>
                                    <th class="head-light">{{__('ПДВ:')}}</th>
                                    <td>0 грн</td>
                                </tr>
                                <tr>
                                    <th class="head-light">{{__('За доставку:')}}</th>
                                    <td>
                                        <span class="check-shipping-method">
                                            {{ $cart->getShippingMethod() }}
                                        </span> {{__('грн')}}
                                    </td>
                                </tr>
                                <tr>
                                    <th class="head-light">{{__('Дисконт:')}}</th>
                                    <td>
                                        <span class="check-discount">0</span> {{__('грн')}}
                                    </td>
                                </tr>
                                <tr>
                                    @if ($cart->getCupon())
                                        <th class="head-light">{{__('Купон:')}}</th>
                                        <td>
                                            <span class="check-cupon">{{$cart->getCupon()}}</span> {{__('грн')}}
                                        </td>
                                    @endif
                                </tr>
                                <tr>
                                    @if ($cart->getGiftCard())
                                        <th class="head-light">{{__('Подарочная карта:')}}</th>
                                        <td>
                                        <span class="check-subtotal-span">
                                            <span class="check-gift-cart">-{{$cart->getGiftCard()}} {{__('грн')}}</span>
                                            <a href="{{URL::to('/shop/removegiftcard')}}"
                                               class="remove-gift-card-button">{{__('Отменить')}}</a>
                                        </span>
                                        </td>
                                    @endif
                                </tr>
                                <tr>
                                    <th class="head-light">{{__('Итоговая стоимость:')}}</th>
                                    <td>
                                        <span
                                            class="check-total-subtotal">{{ $cart->getSubtotal() }}</span> {{__('грн')}}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="checkout-pay">
                            <button type="submit"
                                    class="btn btn-success btn-checkout-end">{{__('Оформить заказ')}}</button>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <script>
            function setTotalForProduct(value) {
                $('.total-products-price' + value).empty();
                $('.total-products-price' + value).append($('#qty-input-value' + value).val() * {{ isset($cartItems['price']) ? $cartItems['price'] : 0 }});
            }

            function changeQty(value) {
                $.ajax({
                    url: '/shop/cart/changeQty/' + value + ',' + $('#qty-input-value' + value).val(),
                    type: 'get',
                    dataType: 'json',
                    success: function (response) {
                        $('#qty-input-value' + value).val(response);
                        setTotalForProduct(value);
                        updateTotal();
                        updateSubtotal();
                    }
                })
            }

            function addQty(value) {
                $.ajax({
                    url: '/shop/cart/addqty/' + value,
                    type: 'get',
                    dataType: 'json',
                    success: function (response) {
                        $('#qty-input-value' + value).val(response);
                        setTotalForProduct(value);
                        updateTotal();
                        updateSubtotal();
                    }
                })
            }

            function minusQty(value) {
                $.ajax({
                    url: '/shop/cart/minusqty/' + value,
                    type: 'get',
                    dataType: 'json',
                    success: function (response) {
                        $('#qty-input-value' + value).val(response);
                        setTotalForProduct(value);
                        updateTotal();
                        updateSubtotal();
                    }
                })
            }

            function updateTotal() {
                $.ajax({
                    url: '/shop/cart/getTotal',
                    type: 'get',
                    dataType: 'json',
                    success: function (response) {
                        $('.check-total').empty();
                        $('.check-total').append(response);
                    }
                });
            }

            function updateSubtotal() {
                $.ajax({
                    url: '/shop/cart/getSubtotal',
                    type: 'get',
                    dataType: 'json',
                    success: function (response) {
                        $('.check-total-subtotal').empty();
                        $('.check-total-subtotal').append(response);
                    }
                });
            }

            $(":radio[name='shippingMethod']").change(function () {
                var radioValue = $(":radio[name='shippingMethod']:checked").val();
                $.ajax({
                    url: '/shop/cart/addShippingMethod/' + radioValue,
                    type: 'get',
                    dataType: 'json',
                    success: function (response) {
                        updateSubtotal();
                        $('.check-shipping-method').empty();
                        $('.check-shipping-method').append(response);
                    }
                });
            });
        </script>
    </div>
@endsection
