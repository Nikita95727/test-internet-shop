@extends('layouts.admin.adminlayout')
@section('content')
    <h3>Импорт пользователей</h3>
    <form method="post" action="{{URL::to('/user/import')}}" enctype="multipart/form-data">
        <div class="form-group">
            {{csrf_field()}}
            <input type="file" name="importFile">
        </div>
        <button type="submit">Импортировать</button>
    </form>
@endsection
