@extends('layouts.admin.adminlayout')
@section('content')
    <div class="row">
        @if (session()->exists('success'))
            <div class="alert alert-success" role="alert">
                {{session()->get('success')}}
            </div>
        @endif
        @if (session()->exists('error'))
            <div class="alert alert-error" role="alert">
                {{session()->get('success')}}
            </div>
        @endif
        <div class="col-4 item-photo">
            @if ($user->avatar)
                <img src="{{asset($user->avatar)}}" width="200px" height="200px"/>
            @else
                <img src="{{asset('img/default-user-avatar.jpg')}}" width="200px" height="200px">
            @endif
        </div>
        <div class="col-md-5">
            <div class="user-name-fio">
                <span>{{__('Полное имя пользователя: '.$user->name)}}</span> <span>{{$user->surname}}</span>
            </div>
            <div class="user-email">
                <span>{{__('Электронный адрес: '.$user->email)}}</span>
            </div>
            <div class="user-birthday-date">
                <span>{{__('Дата рождения: '.$user->customer_birthdate)}}</span>
            </div>
            <div class="user-shipping-addresses">
                <span>{{__('Адрес(а) пользователя:')}}</span>
            </div>
            @if (Auth::user()->isAdmin())
                <div class="user-role">
                    <span>{{__('Роль пользователя: '.$user->getRoleName())}} <a href="#" data-toggle="modal" data-target="#editRoleModal"><i class="fas fa-user-cog"></i></a></span>
                    <div class="modal" tabindex="-1" id="editRoleModal" aria-labelledby="exampleModalLabel" aria-hidden="true" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">{{__('Изменение роли')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="post" action="{{URL::to('/shop/admin/user/changeRole')}}">
                                    <div class="modal-body">
                                        @csrf
                                        <input type="hidden" name="userId" value="{{$user->id}}">
                                        <select class="form-control" name="userRole">
                                            @foreach($user->getUserRoles() as $role)
                                                <option value="{{$role->id}}">{{$role->roleName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">{{__('Назначить')}}</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Отмена')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <table class="table table-bordered table-info">
            <thead>
            <th>{{__('Номер заказа')}}</th>
            <th>{{__('Товары')}}</th>
            <th>{{__('Общая сумма')}}</th>
            <th>{{__('Статус')}}</th>
            <th>{{__('Отследить')}}</th>
            </thead>
        </table>
    </div>
@endsection
