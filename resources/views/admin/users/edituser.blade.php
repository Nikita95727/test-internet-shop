@extends('layouts.admin.adminlayout')
@section('content')
    <h2 class="customer-reg-form-header">{{__('admin.edit_user')}}</h2>
    <p class="customer-reg-form-additional-information">{{__('Пожалуйста, введите данные для создания учётной записи')}}</p>
    <form action="{{URL::to('/shop/admin/user/store')}}" method="post" class="customer-reg-form">
        @csrf
        <input type="hidden" value="{{$user->id}}" name="userId">
        <label for="name">{{__('Имя')}}<em>*</em></label>
        <input id="name" type="text" class="form-control" name="name" value="{{$user->name}}" required autofocus>
        <label for="surname">{{__('Фамилия')}}<em>*</em></label>
        <input id="surname" type="text" class="form-control" name="surname" value="{{$user->surname}}" required autofocus>
        <label for="customer_birthdate">{{__('Дата рождения')}}<em>*</em></label>
        <input id="customer_birthdate" type="date" class="form-control" name="customer_birthdate" value="{{$user->customer_birthdate}}" required>
        <label for="email">{{__('E-mail')}}<em>*</em></label>
        <input id="email" type="email" class="form-control" name="email" value="{{$user->email}}" required>
        <div class="g-recaptcha" data-sitekey="6LfMyK0UAAAAANq227L_e8-_cssMb3HcdNzGDt57"></div>
        <button type="submit" class="form-control btn customer-reg-submit">{{__('Редактировать')}}</button>
    </form>
@endsection
