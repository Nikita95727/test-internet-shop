@extends('layouts.admin.adminlayout')
@section('content')
    <div class="action-item">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="{{URL::to('/shop/admin/createUser')}}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ __('admin.create_user') }}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="action-item">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="{{URL::to('/shop/admin/showUsers')}}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ __('admin.users_management') }}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="action-item">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="{{URL::to('/shop/admin/user/manageRoles')}}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ __('admin.users_roles_management') }}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="action-item">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="{{URL::to('/shop/admin/users/import')}}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ __('admin.import_users') }}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="action-item">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="{{URL::to('/shop/admin/users/export')}}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ __('admin.export_users') }}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
@endsection
