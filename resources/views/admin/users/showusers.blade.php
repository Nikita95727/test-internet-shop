@extends('layouts.admin.adminlayout')
@section('content')
    <div class="card shadow mb-4">
        @if(session()->exists('success'))
            <div class="alert alert-success" role="alert">
                {{session()->get('success')}}
            </div>
        @endif
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('text.name') }}</th>
                    <th>{{ __('text.surname') }}</th>
                    <th>{{ __('text.birthdate') }}</th>
                    <th>{{ __('text.email') }}</th>
                    <th>{{ __('text.address') }}</th>
                    <th>{{ __('text.actions') }}</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>#</th>
                    <th>{{ __('text.name') }}</th>
                    <th>{{ __('text.surname') }}</th>
                    <th>{{ __('text.birthdate') }}</th>
                    <th>{{ __('text.email') }}</th>
                    <th>{{ __('text.address') }}</th>
                    <th>{{ __('text.actions') }}</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->surname}}</td>
                        <td>{{$user->customer_birthdate}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->customer_shippingAddress}}</td>
                        <td><a href="#"><i class="fas fa-user-minus" title="{{__('Удалить пользователя')}}"></i></a><br><a href="{{URL::to('/shop/admin/editUser/'.$user->id)}}"><i class="fas fa-user-edit" title="{{__('Изменить')}}"></i></a><a href="{{URL::to('/shop/admin/users/showUser/'.$user->id)}}"><i class="fas fa-user-cog"></i></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <a href="{{ URL::to('/shop/admin/createUser') }}"><h6><i class="fas fa-user-plus" title="{{__('Добавить пользователя')}}"></i></h6></a>
        {{$users->links()}}
    </div>
@endsection
