@extends('layouts.admin.adminlayout')
@section('content')
    <h2>{{ __('admin.banner_design') }}</h2>
    <a href="{{URL::to('/shop/admin/design/createbanner')}}" class="btn btn-success"><h6>{{ __('admin.add_banner') }}</h6></a>
@endsection
