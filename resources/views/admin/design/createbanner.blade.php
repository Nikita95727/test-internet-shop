@extends('layouts.admin.adminlayout')
@section('content')
    <h2>{{ __('admin.add_banner') }}</h2>
    <form action="{{ URL::to('/shop/admin/design/storebanner') }}" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="bannerName">{{ __('admin.banner_name') }}</label>
            <input type="text" class="form-control" name="bannerName">
        </div>
        <div class="form-group">
            <label for="bannerImages">{{ __('admin.banner_image') }}</label>
            <input type="file" class="form-control" name="bannerImages[]" id="bannerImage">
        </div>
        <div class="form-group">
            <button type="button" class="btn btn-info" onclick="addImageField()">{{ __('admin.add_image') }}</button>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success">{{ __('actions.save') }}</button>
        </div>
    </form>
    <script>
        function addImageField() {
            $('#bannerImage').after('<label for="documentImage">{{ __('admin.banner_image') }}</label><input type="file" name="bannerImages[]" class="form-control">');
        }
    </script>
@endsection
