@extends('layouts.admin.adminlayout')
@section('content')
    <div class="vendor-info">
        <h4>{{__('Конфигурация магазина')}}</h4>
        @if ($vendors)
            @if (session()->exists('success'))
                <div class="alert alert-success" role="alert">
                    {{session()->get('success')}}
                </div>
            @endif
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">{{__('#')}}</th>
                    <th scope="col">{{__('Наименование производителя')}}</th>
                    <th scope="col">{{__('Действие')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($vendors as $vendor)
                    <tr>
                        <td>{{$vendor->id}}</td>
                        <td>{{$vendor->vendor_name}}</td>
                        <td><a href="#" class="btn btn-danger" data-toggle="modal" data-target="#deleteVendor{{$vendor->id}}"><i class="fas fa-trash"></i></a></td>
                        <div class="modal fade" id="deleteVendor{{$vendor->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">{{__('Удалить производителя?')}}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <span>{{__('Вы уверены, что хотите удалить производителя?')}}</span>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Закрыть')}}</button>
                                        <a href="{{URL::to('/admin/deleteVendor/'.$vendor->id)}}"><button type="button" class="btn btn-primary">{{__('Удалить производителя')}}</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#addVendor">{{__('Добавить производителя')}}</a>
            <div class="modal fade" id="addVendor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">{{__('Добавить производителя')}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="post" action="{{URL::to('/admin/storeVendor')}}">
                            <div class="modal-body">
                                <label for="vendorName">{{__('Наименование производителя')}}</label>
                                <input type="text" class="form-control" name="vendorName">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Закрыть')}}</button>
                                <button type="submit" class="btn btn-primary">{{__('Добавить производителя')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
