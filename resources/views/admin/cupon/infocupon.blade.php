@extends('layouts.admin.adminlayout')
@section('content')
    <div class="card shadow mb-4">
        <?php if(session()->exists('success')): ?>
        <div class="alert alert-success" role="alert">
            {{session()->get('success')}}
        </div>
        <?php endif; ?>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Код купона</th>
                    <th>Номинал</th>
                    <th>Статус</th>
                    <th></th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>#</th>
                    <th>Код купона</th>
                    <th>Номинал</th>
                    <th>Статус</th>
                    <th></th>
                </tr>
                </tfoot>
                <tbody>
                @foreach ($cupons as $cupon)
                    <tr>
                        <td>{{$cupon->id}}</td>
                        <td>{{$cupon->cuponCode}}</td>          <!--поля минграции -->
                        <td>{{$cupon->nominal}}</td>
                        <td>@if($cupon->active)
                                {{ __('text.yes') }}
                            @else
                                {{ __('text.no') }}
                            @endif
                        </td>
                        <td><a href="{{URL::to('/shop/deletecupon/')}}/{{$cupon->id}}" class="btn btn-danger"><i class="fas fa-trash"></i></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <a href="{{URL::to('/shop/admin/addcupon')}}" class="btn btn-success"><h6>Добавить купон</h6></a>
        {{$cupons->links()}}
    </div>
@endsection
