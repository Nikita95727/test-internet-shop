@extends('layouts.admin.adminNavigation')
@section('content')
    <div class="col-md-12">
        <form method="post" action="{{URL::to('/shop/admin/savecupon')}}" class="product-create-form">
            {{csrf_field()}}
            <div class="form-group">
                <label for="cuponcode" title="Обязательное поле">Код карточной скидки*</label>
                <input type="text" class="form-control" name="cuponcode" maxlength="12" required>
            </div>
            <div class="form-group">
                <label for="nominal" title="Обязательное поле">Номинал*</label>
                <input type="text" class="form-control" name="nominal" maxlength="12" required>
            </div>
            <div class="form-group">
                <label for="selectstatus" title="Обязательное поле">Статус*</label>
                <select class="form-control" name="selectstatus">
                    <option>active</option>
                    <option>inactive</option>
                </select>
            </div>
            <button type="submit" class="btn btn-success">Добавить карту</button>
        </form>
    </div>
@endsection
