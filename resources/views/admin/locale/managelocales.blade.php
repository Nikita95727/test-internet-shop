@extends('layouts.admin.adminlayout')
@section('content')
    <div class="control-panel">
        <a href="{{ URL::to('/admin/managelocale/createlocale') }}" class="btn btn-success" title="{{ __('admin.add_new_locale') }}"><i class="fas fa-plus-square"></i></a>
    </div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>{{ __('admin.locale_name') }}</th>
            <th>{{ __('admin.locale_code') }}</th>
            <th>{{ __('admin.locale_flag') }}</th>
            <th>{{ __('actions.actions') }}</th>
        </tr>
        </thead>
        <tbody>
        @if($locales->isNotEmpty())
            @foreach($locales as $locale)
                <tr>
                    <td>{{ $locale->localeName }}</td>
                    <td>{{ $locale->localeCode }}</td>
                    <td>
                        <img src="{{ asset('/storage/'.$locale->localeFlag) }}" class="locale-img">
                    </td>
                    <td>
                        <a href="{{ URL::to('/admin/managelocale/delete/'.$locale->id) }}" class="btn btn-danger">
                            <i class="fas fa-trash"></i>
                        </a>
                        <a href="{{ URL::to('/admin/managelocale/edit/'.$locale->id) }}" class="btn btn-primary">
                            <i class="fas fa-edit"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@endsection

