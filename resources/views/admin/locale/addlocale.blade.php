@extends('layouts.admin.adminlayout')
@section('content')
    <form action="{{ URL::to('/admin/managelocale/save') }}" method="post" enctype="multipart/form-data">
        <label for="localeName">{{ __('admin.locale_name') }}</label>
        <input type="text" class="form-control" name="localeName">
        <label for="localeCode">{{ __('admin.locale_code') }}</label>
        <input type="text" class="form-control" name="localeCode">
        <label for="localeFlag">{{ __('admin.locale_flag') }}</label>
        <input type="file" name="localeFlag" class="form-control">
        <button type="submit" class="btn btn-success">{{ __('actions.save') }}</button>
    </form>
@endsection
