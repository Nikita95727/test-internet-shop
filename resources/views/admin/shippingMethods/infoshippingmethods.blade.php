@extends('layouts.admin.adminlayout')
@section('content')
    <?php if(session()->exists('success')): ?>
    <div class="alert alert-success" role="alert">
        {{session()->get('success')}}
    </div>
    <?php endif; ?>
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>#</th>
                <th>{{__('admin.delivery_methods')}}</th>
                <th>{{__('admin.delivery_description')}}</th>
                <th>{{__('admin.delivery_price')}}</th>
                <th>{{__('text.status')}}</th>
                <th>{{__('text.actions')}}</th>
                <th></th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>#</th>
                <th>{{__('admin.delivery_methods')}}</th>
                <th>{{__('admin.delivery_description')}}</th>
                <th>{{__('admin.delivery_price')}}</th>
                <th>{{__('text.status')}}</th>
                <th>{{__('text.actions')}}</th>
                <th></th>
            </tr>
            </tfoot>
            <tbody>
            @foreach ($shippments as $shipping)
            <tr>
                <td>{{$shipping->id}}</td>
                <td>{{$shipping->shippingMethodName}}</td>
                <td>{{$shipping->shippingMethodDescription}}</td>
                <td>{{$shipping->shippingMethodPrice}}</td>
                <td>
                    @if ($shipping->shippingMethodStatus)
                        {{ __('text.active') }}
                    @else
                        {{ __('text.not_active') }}
                    @endif
                </td>
                <td><a href="{{URL::to('/shop/deleteshipping/')}}/{{$shipping->id}}" class="btn btn-danger"><i class="fas fa-trash"></i></a> <a href="{{URL::to('/shop/admin/editshippingmethods')}}/{{$shipping->id}}" class="btn btn-dark"><i class="fas fa-edit"></i></a></td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <a href="{{URL::to('/shop/admin/addshippingmethods')}}" class="btn btn-info">{{__('admin.add_new_delivery_method')}}</a>{{$shippments->links()}}
@endsection
