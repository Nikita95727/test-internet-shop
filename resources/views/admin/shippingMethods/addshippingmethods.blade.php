@extends('layouts.admin.adminlayout')
@section('content')
    <h2>{{ __('admin.add_delivery_method') }}</h2>
    <form method="post" action="{{URL::to('/shop/admin/saveshipping')}}">
        {{csrf_field()}}
        <div>
            <div class="form-group">
                <label for="posta">{{__('admin.delivery_method_name')}}</label>
                <input type="text" class="form-control" name="shippingMethodName">
            </div>
            <div class="form-group">
                <label for="description">{{__('admin.delivery_description')}}</label>
                <textarea type="text" class="form-control" name="shippingMethodDescription"></textarea>
            </div>
            <div class="form-group">
                <label for="price">{{__('admin.delivery_price')}}</label>
                <input type="text" class="form-control" name="shippingMethodPrice">
            </div>
            <div class="form-group">
                <label for="selectstatus" title="Обязательное поле">{{__('text.status')}}*</label>
                <select class="form-control" name="shippingMethodStatus">
                    <option value="1">{{ __('text.active') }}</option>
                    <option value="0">{{ __('text.not_active') }}</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">{{__('actions.save')}}</button>
        </div>
    </form>
@endsection
