@extends('layouts.admin.adminlayout')
@section('content')
    <div class="action-item">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="{{URL::to ('/shop/admin/infogiftcard')}}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Управление подарочными картами</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="action-item">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="#">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Управление дисконтными картами</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="action-item">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="{{URL::to ('/shop/admin/infocupon')}}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Управление купонами</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="action-item">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="{{URL::to ('/shop/admin/managediscounts')}}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ __('admin.manage_discounts') }}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
@endsection
