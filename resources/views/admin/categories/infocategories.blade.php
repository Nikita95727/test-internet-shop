@extends('layouts.admin.adminlayout')
@section('content')
    <div class="table-responsive">
        @if(session()->exists('success'))
            <div class="alert alert-success" role="alert">
                {{session()->get('success')}}
            </div>
        @endif
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th width="5%">#</th>
                <th>{{ __('admin.category_name') }}</th>
                <th>{{ __('admin.category_code') }}</th>
                <th>{{ __('admin.parent_category') }}</th>
                <th width="13%"></th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th width="5%">#</th>
                <th>{{ __('admin.category_name') }}</th>
                <th>{{ __('admin.category_code') }}</th>
                <th>{{ __('admin.parent_category') }}</th>
                <th width="13%"></th>
            </tr>
            </tfoot>
            <tbody>
            @foreach ($categories as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->categoryName }}</td>
                    <td>{{ $category->categoryCode  }}</td>
                    <td>{{ $category->getParentCategoryName() }}</td>
                    <td>
                        <a href="{{URL::to('/shop/admin/deletecategory/'.$category->id)}}" class="btn btn-danger"><i class="fas fa-trash"></i>
                        </a>
                        <a href="#" class="btn btn-dark" data-toggle="modal" data-target="#editCategoryModal{{$category->id}}">
                            <i class="fas fa-edit"></i>
                        </a>
                        <div class="modal fade" id="editCategoryModal{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">{{ __('admin.edit_category') }}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post" action="{{URL::to('/shop/storeCategory')}}">
                                        <div class="modal-body">
                                            <input type="hidden" value="{{$category->id}}" name="categoryId">
                                            <label for="categoryName">{{ __('admin.category_name') }}</label>
                                            <input type="text" class="form-control" name="categoryName" value="{{$category->categoryName}}">
                                            <label for="categoryName">{{ __('admin.category_code') }}</label>
                                            <input type="text" class="form-control" name="categoryCode" value="{{$category->categoryCode}}">
                                            <label for="parentId">{{ __('admin.parent_category') }}</label>
                                            <select type="text" class="form-control" name="parentId">
                                                <option></option>
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}">{{ $category->categoryName }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('actions.close') }}</button>
                                            <button type="submit" class="btn btn-primary">{{ __('actions.save') }}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
            {{ $categories->links() }}
        </table>
        <a href="#" data-toggle="modal" data-target="#createCategoryModal">{{ __('admin.add_category') }}</a>
        <div class="modal fade" id="createCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{ __('admin.add_category') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="{{URL::to('/shop/storeCategory')}}">
                        <div class="modal-body">
                            <label for="categoryName">{{ __('admin.category_name') }}</label>
                            <input type="text" class="form-control" name="categoryName">
                            <label for="categoryCode">{{ __('admin.category_code') }}</label>
                            <input type="text" class="form-control" name="categoryCode">
                            <label for="parentId">{{ __('admin.parent_category') }}</label>
                            <select type="text" class="form-control" name="parentId">
                                <option></option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->categoryName }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('actions.close') }}</button>
                            <button type="submit" class="btn btn-primary">{{ __('actions.save') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
