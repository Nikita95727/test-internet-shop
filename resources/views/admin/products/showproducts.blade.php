@extends('layouts.admin.adminlayout')
@section('content')
    @if(session()->exists('success'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success')}}
        </div>
    @endif
    <div class="card shadow mb-4">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Наименование')}}</th>
                    <th>{{__('Категория')}}</th>
                    <th>{{__('Производитель')}}</th>
                    <th>{{__('Описание')}}</th>
                    <th>{{__('Картинка')}}</th>
                    <th>{{__('Цена')}}</th>
                    <th></th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>#</th>
                    <th>{{__('Наименование')}}</th>
                    <th>{{__('Категория')}}</th>
                    <th>{{__('Производитель')}}</th>
                    <th>{{__('Описание')}}</th>
                    <th>{{__('Картинка')}}</th>
                    <th>{{__('Цена')}}</th>
                    <th></th>
                </tr>
                </tfoot>
                <tbody>
                @foreach ($products as $product)
                    <tr>
                        <td>{{ $product->getId() }}</td>
                        <td>{{ $product->getTitle() }}</td>
                        <td>{{ $product->getCategory() }}
                        </td>
                        <td>{{$product->getVendor()}}</td>
                        <td>{{$product->getDescription()}}</td>
                        <td><img src="{{$product->getImage()}}" width="64px" heigth="64px"></td>
                        <td>{{$product->getPrice()}}</td>
                        <td><a href="{{URL::to('/shop/product/delete/'.$product->getId())}}" class="btn btn-danger"><i class="fas fa-trash"></i></a><a href="{{URL::to('/shop/product/edit/'.$product->getId())}}" class="btn btn-dark"><i class="fas fa-edit"></i></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <a href="{{URL::to('/shop/admin/createItem')}}"><h6>{{__('Добавить новый товар')}}</h6></a>
        {{$products->links()}}
    </div>
@endsection
