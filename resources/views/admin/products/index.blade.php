@extends('layouts.admin.adminlayout')
@section('content')
    <div class="action-item">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="{{URL::to('/shop/admin/createItem')}}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ __('admin.add_product') }}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="action-item">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="{{URL::to('/shop/admin/showItems')}}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ __('admin.products_list') }}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="action-item">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="{{URL::to('/admin/importproducts')}}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ __('admin.import_products') }}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="action-item">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="{{URL::to('/shop/admin/export')}}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ __('admin.export_products') }}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="action-item">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="{{URL::to('/shop/admin/managecategories')}}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ __('admin.manage_categories') }}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="action-item">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="{{URL::to('/admin/manageVendors')}}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{__('admin.manufacturers_management')}}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="action-item">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="{{URL::to('/admin/manageArticles')}}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{__('admin.articles')}}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
@endsection
