@extends('layouts.admin.adminlayout')
@section('content')
    <h3>Импорт продуктов</h3>
    <form method="post" action="{{URL::to('/item/import')}}" enctype="multipart/form-data">
        <div class="form-group">
            {{csrf_field()}}
            <input type="file" name="importFile" class="form-control-file">
        </div>
        <button type="submit" class="btn btn-primary">{{__('Импортировать')}}</button>
    </form>
@endsection
