@extends('layouts.admin.adminlayout')
@section('content')
    <form method="post" action="{{URL::to('/item/store')}}" class="product-create-form">
        {{csrf_field()}}
        <input type="hidden" value="{{$product->getId()}}" name="productId">
        <div class="form-group">
            <label for="title" title="Обязательное поле">Заголовок*</label>
            <input type="text" class="form-control" name="title" value="{{$product->getTitle()}}" required>
        </div>
        <div class="form-group">
            <label for="category" title="Обязательное поле">Категория*</label>
            <select class="form-control" name="productCategory">
                @foreach ($categories as $category)
                    <option value="{{$category->id}}">{{$category->category_name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="category" title="Обязательное поле">Производитель*</label>
            <select class="form-control" name="productVendor">
                @foreach ($vendors as $vendor)
                    <option value="{{$vendor->id}}">{{$vendor->vendor_name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="description" title="Обязательное поле">Описание*</label>
            <textarea class="form-control" name="description">
                    {{$product->getDescription()}}
                </textarea>
        </div>
        <div class="form-group">
            <label for="image" title="Обязательное поле">Изображение*</label>
            <input type="text" class="form-control" name="image" value="{{$product->getImage()}}" required>
        </div>
        <div class="form-group">
            <label for="price" title="Обязательное поле">Price*</label>
            <input type="text" class="form-control" name="price" value="{{$product->getPrice()}}" required>
        </div>
        <button type="submit" class="btn btn-success">Добавить товар</button>
    </form>
@endsection
