@extends('layouts.admin.adminlayout')
@section('content')
    <form method="post" action="{{URL::to('/item/store')}}" class="product-create-form">
        {{csrf_field()}}
        <div class="form-group">
            <label for="title" title="Обязательное поле">Заголовок*</label>
            <input type="text" class="form-control" name="title" required>
        </div>
        <div class="form-group">
            <label for="category" title="Обязательное поле">Категория*</label>
            <select class="form-control" name="productCategory">
                @foreach ($categories as $category)
                    <option value="{{$category->id}}">{{$category->categoryName}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="category" title="Обязательное поле">Производитель*</label>
            <select class="form-control" name="productVendor">
                @foreach ($vendors as $vendor)
                    <option value="{{$vendor->id}}">{{$vendor->vendor_name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="description" title="Обязательное поле">Описание*</label>
            <textarea class="form-control" name="description"></textarea>
        </div>
        <div class="form-group">
            <label for="image" title="Обязательное поле">Изображение*</label>
            <input type="text" class="form-control" name="image" required>
        </div>
        <div class="form-group">
            <label for="price" title="Обязательное поле">Price*</label>
            <input type="text" class="form-control" name="price" required>
        </div>
        <div class="form-group attributes">
            <label for="attributes">Основные атрибуты</label>
            <input type="text" class="form-control" name="attributes[]" required>
            <div class = 'add-attribute-btn-container'>
                <a href="#" class="add-attribute-btn">{{__('Добавить атрибут')}}</a>
            </div>
        </div>
        <button type="submit" class="btn btn-success">Добавить товар</button>
    </form>
    <script>
        $('.add-attribute-btn').click(function(event) {
            addDynamicExtraField();
            return false;
        });

        function addDynamicExtraField() {
            $('<input type="text" name="attributes[]" class="form-control">').appendTo($('.attributes'));
        }
    </script>
@endsection
