@extends('layouts.admin.adminlayout')
@section('content')
    <div class="card shadow mb-4">
        <?php if(session()->exists('success')): ?>
        <div class="alert alert-success" role="alert">
            {{session()->get('success')}}
        </div>
        <?php endif; ?>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Код карточной скидки</th>
                    <th>Номинал</th>
                    <th>Статус</th>
                    <th></th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>#</th>
                    <th>Код карточной скидки</th>
                    <th>Номинал</th>
                    <th>Статус</th>
                    <th></th>
                </tr>
                </tfoot>
                <tbody>
                @foreach ($giftcards as $giftcard)
                <tr>
                    <td>{{$giftcard->id}}</td>
                    <td>{{$giftcard->giftCardCode}}</td>
                    <td>{{$giftcard->nominal}}</td>
                    <td>@if($giftcard->active)
                            {{ __('text.yes') }}
                        @else
                            {{ __('text.no') }}
                        @endif
                    </td>
                    <td><a href="{{URL::to('/shop/deletegiftcard/')}}/{{$giftcard->id}}" class="btn btn-danger"><i class="fas fa-trash"></i></a></td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <a href="{{URL::to('/shop/admin/addgiftcard')}}" class="btn btn-success"><h6>Добавить новую карту</h6></a>
        {{$giftcards->links()}}
    </div>
@endsection
