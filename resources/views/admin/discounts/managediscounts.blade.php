@extends('layouts.admin.adminlayout')
@section('content')
    <h4>{{ __('admin.manage_discounts') }}</h4>
    <div class="control-panel">
        <a href="#" class="btn btn-success" data-toggle="modal" data-target="#createDiscountModal">{{ __('admin.add_discount') }}</a>
        <div class="modal fade" id="createDiscountModal" tabindex="-1" role="dialog" aria-labelledby="createDiscountModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="createDiscountModalLabel">{{ __('admin.add_discount') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="{{URL::to('/shop/admin/managediscounts/store')}}">
                        <div class="modal-body">
                            <label for="product">{{ __('admin.product') }}</label>
                            <select type="text" class="form-control" name="product">
                                <option></option>
                                @foreach($products as $product)
                                    <option value="{{ $product->id }}">{{ $product->title }}</option>
                                @endforeach
                            </select>
                            <label for="discountValue">{{ __('text.discount_value') }}</label>
                            <input type="text" class="form-control" name="discountValue">
                            <label for="expirationDate">{{ __('text.expiration_date') }}</label>
                            <input type="date" class="form-control" name="expirationDate">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('actions.close') }}</button>
                            <button type="submit" class="btn btn-primary">{{ __('actions.save') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="admin-content">
        @if($discounts->isNotEmpty())
            <table class="table table-bordered">
                <thead>
                    <th>{{ __('text.product') }}</th>
                    <th>{{ __('text.discount_value') }}</th>
                    <th>{{ __('text.expiration_date') }}</th>
                    <th>{{ __('actions.actions') }}</th>
                </thead>
                <tbody>
                    @foreach($discounts as $discount)
                        <tr>
                            <td>{{ $discount->getProductTitle() }}</td>
                            <td>{{ $discount->discountValue }}%</td>
                            <td>{{ $discount->expirationDate }}</td>
                            <td>
                                <a href="{{ URL::to('/shop/admin/managediscounts/'.$discount->id) }}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
@endsection
