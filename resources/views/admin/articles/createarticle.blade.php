@extends('layouts.admin.adminlayout')
@section('content')
    <h3>{{ __('admin.create_article') }}</h3>
    <form action="{{ URL::to('/admin/manageArticles/store') }}" method="POST">
        <label for="articleTitle">{{ __('admin.article_title') }}</label>
        <input type="text" class="form-control" name="articleTitle" required>
        <label for="articleDescription">{{ __('admin.article_description') }}</label>
        <textarea class="form-control" name="articleDescription" required></textarea>
        <label for="articleStatus">{{ __('admin.article_status') }}</label>
        <select name="articleStatus" class="form-control" required>
            <option></option>
            <option value="0">{{ __('admin.status_disabled') }}</option>
            <option value="1">{{ __('admin.status_active') }}</option>
        </select>
        <button type="submit" class="btn btn-success">{{ __('actions.save') }}</button>
    </form>
@endsection
