@extends('layouts.admin.adminlayout')
@section('content')
    <div class="control-panel">
        <a href="{{ URL::to('/admin/manageArticles/create') }}" class="btn btn-success">{{ __('admin.create_article') }}</a>
    </div>
    @if($articles)
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>{{ __('admin.article_title') }}</th>
                    <th>{{ __('admin.article_description') }}</th>
                    <th>{{ __('admin.article_status') }}</th>
                    <th>{{ __('actions.actions') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($articles as $article)
                    <tr>
                        <td>{{ $article->title }}</td>
                        <td>{{ $article->description }}</td>
                        <td>
                            @if($article->status)
                                {{ __('admin.status_active') }}
                            @else
                                {{ __('admin.status_disabled') }}
                            @endif
                        </td>
                        <td>
                            <a href="{{ URL::to('/admin/manageArticles/remove/'.$article->id) }}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                            <a href="#" class="btn btn-info"><i class="fas fa-edit"></i></a>
                            @if($article->status)
                                <a href="{{ URL::to('/admin/manageArticles/changeStatus/'.$article->id) }}" class="btn btn-success"><i class="fas fa-check-circle"></i></a>
                            @else
                                <a href="{{ URL::to('/admin/manageArticles/changeStatus/'.$article->id) }}" class="btn btn-light"><i class="fas fa-check-circle"></i></a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
@endsection
