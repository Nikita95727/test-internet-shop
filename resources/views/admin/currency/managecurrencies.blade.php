@extends('layouts.admin.adminlayout')
@section('content')
    <div class="card shadow mb-4">
        <?php if(session()->exists('success')): ?>
        <div class="alert alert-success" role="alert">
            {{session()->get('success')}}
        </div>
        <?php endif; ?>
        <div class="table-responsive">
            <div class="action-panel">
                <a href="{{ URL::to('/shop/admin/managecurrencies/updatecurrencies') }}" title="{{ __('Обновить курсы валют') }}" class="refresh-btn"><i class="fas fa-sync-alt"></i></a>
                <a href="{{ URL::to('/shop/admin/managecurrencies/add') }}" title="{{ __('Добавить валюту') }}" class="add-btn"><i class="fas fa-plus-square"></i></a>
            </div>
            <table class="table table-bordered" id="dataTable">
                <thead>
                <tr>
                    <th>{{ __('Название валюты') }}</th>
                    <th>{{ __('Код') }}</th>
                    <th>{{ __('Значение') }}</th>
                    <th>{{ __('По-умолчанию') }}</th>
                    <th>{{ __('Дата обновления') }}</th>
                    <th>{{ __('Действие') }}</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>{{ __('Название валюты') }}</th>
                    <th>{{ __('Код') }}</th>
                    <th>{{ __('Значение') }}</th>
                    <th>{{ __('По-умолчанию') }}</th>
                    <th>{{ __('Дата обновления') }}</th>
                    <th>{{ __('Действие') }}</th>
                </tr>
                </tfoot>
                <tbody>
                @if(isset($currencies))
                    @foreach($currencies as $currency)
                        <tr>
                            <td>{{ $currency->currencyName }}</td>
                            <td>{{ $currency->currencyCode }}</td>
                            <td>{{ $currency->currencyValue }}</td>
                            <td>
                                @if($currency->default)
                                    {{ __('text.yes') }}
                                @else
                                    {{ __('text.no') }}
                                @endif
                            </td>
                            <td>{{ $currency->updated_at}}</td>
                            <td>
                                <a href="{{ URL::to('/shop/admin/managecurrrencies/remove/'.$currency->id) }}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                @if($currency->default)
                                    <a href="{{ URL::to('/shop/admin/managecurrencies/setAsDefault/'.$currency->id) }}" class="btn btn-success"><i class="fas fa-check-circle"></i></a>
                                @else
                                    <a href="{{ URL::to('/shop/admin/managecurrencies/setAsDefault/'.$currency->id) }}" class="btn btn-light"><i class="fas fa-check-circle"></i></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
