@extends('layouts.admin.adminlayout')
@section('content')
    <div class="col-md-12">
        <form method="post" action="{{URL::to('/shop/admin/managecurrencies/save')}}" class="currency-create-form">
            {{csrf_field()}}
            <div class="form-group">
                <label for="currencyName" title="Обязательное поле">{{ __('Название валюты') }}</label>
                <input type="text" class="form-control" name="currencyName" maxlength="30" required>
                @error('currencyName')
                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="currencyCode" title="Обязательное поле">{{ __('Код') }}</label>
                <input type="text" class="form-control" name="currencyCode" maxlength="4" required>
                @error('currencyCode')
                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="currencySymbol" title="Обязательное поле">{{ __('Символ') }}</label>
                <input type="text" class="form-control" name="currencySymbol" maxlength="1" required>
                @error('currencySymbol')
                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="currencyValue" title="Обязательное поле">{{ __('Значение') }}</label>
                <input type="text" class="form-control" name="currencyValue" required>
                @error('currencyValue')
                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="currencyStatus" title="Обязательное поле">{{ __('Статус') }}</label>
                <select class="form-control" name="currencyStatus">
                    <option value="1">{{ __('Включено') }}</option>
                    <option value="0">{{ __('Отключено') }}</option>
                </select>
            </div>
            <button type="submit" class="btn btn-success">{{ __('Добавить валюту') }}</button>
        </form>
    </div>
@endsection
