@extends('layouts.admin.adminlayout')
@section('content')
<div class="store-config">
    <h4>{{__('Конфигурация магазина')}}</h4>
    @if ($shopconfig)
        @if (session()->exists('success'))
            <div class="alert alert-success" role="alert">
                {{session()->get('success')}}
            </div>
        @endif
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">{{__('Имя магазина')}}</th>
                <th scope="col">{{__('Телефон магазина')}}</th>
                <th scope="col">{{__('Логотип магазина')}}</th>
                <th scope="col">{{__('Электронная почта')}}</th>
                <th scope="col">{{__('Адрес')}}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="shop-config-name">
                    {{$shopconfig->shopName}}
                    <a href="#" data-toggle="modal" data-target="#editShopName{{$shopconfig->id}}">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                    <div class="modal fade" id="editShopName{{$shopconfig->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{__('Изменить имя магазина')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="post" action="{{URL::to('/shop/storeShopName')}}">
                                    <div class="modal-body">
                                        <input type="hidden" value="{{$shopconfig->id}}" name="shopconfigId">
                                        <label for="shopConfigName">{{__('Имя магазина')}}</label>
                                        <input type="text" class="form-control" value="{{$shopconfig->shopName}}" name="shopConfigName">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Закрыть')}}</button>
                                        <button type="submit" class="btn btn-primary">{{__('Сохранить имя')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </td>
                <td class="shop-config-phone">
                    {{$shopconfig->shopPhoneNumber}}<br>
                    <a href="#" data-toggle="modal" data-target="#editShopPhoneNumber{{$shopconfig->id}}">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                    <div class="modal fade" id="editShopPhoneNumber{{$shopconfig->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{__('Изменить номер телефона')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="post" action="{{URL::to('/shop/storeShopPhoneNumber')}}">
                                    <div class="modal-body">
                                        <input type="hidden" value="{{$shopconfig->id}}" name="shopconfigId">
                                        <label for="shopConfigName">{{__('Номер телефона')}}</label>
                                        <input type="text" class="form-control" value="{{$shopconfig->shopPhoneNumber}}" name="shopConfigPhoneNumber">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Закрыть')}}</button>
                                        <button type="submit" class="btn btn-primary">{{__('Сохранить телефон')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </td>
                <td class="shop-config-logo">
                    <img src="{{asset($shopconfig->shopLogo)}}" width="64px" height="64px">
                    <a href="#" data-toggle="modal" data-target="#editShopLogo{{$shopconfig->id}}">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                    <div class="modal fade" id="editShopLogo{{$shopconfig->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{__('Изменить лого')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="post" action="{{URL::to('/shop/storeShopLogo')}}">
                                    <div class="modal-body">
                                        <input type="hidden" value="{{$shopconfig->id}}" name="shopconfigId">
                                        <label for="shopConfigLogo">{{__('Логотип магазина')}}</label>
                                        <img src="{{asset($shopconfig->shopLogo)}}" width="64px" height="64px">
                                        <input type="text" class="form-control" name="shopConfigLogo" placeholder="{{__('Введите ссылку на логотип')}}">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Закрыть')}}</button>
                                        <button type="submit" class="btn btn-primary">{{__('Сохранить лого')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </td>
                <td class="shop-config-address">
                    {{$shopconfig->shopAddress}}<a href="#" data-toggle="modal" data-target="#editShopAddress{{$shopconfig->id}}">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                    <div class="modal fade" id="editShopAddress{{$shopconfig->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{__('Изменить адрес магазина')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="post" action="{{URL::to('/shop/storeShopAddress')}}">
                                    <div class="modal-body">
                                        <input type="hidden" value="{{$shopconfig->id}}" name="shopconfigId">
                                        <label for="shopConfigName">{{__('Адрес магазина')}}</label>
                                        <input type="text" class="form-control" value="{{$shopconfig->shopAddress}}" name="shopConfigAddress">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Закрыть')}}</button>
                                        <button type="submit" class="btn btn-primary">{{__('Сохранить адрес')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </td>
                <td class="shop-email-config">
                    {{$shopconfig->shopEmail}}<a href="#" data-toggle="modal" data-target="#editShopEmail{{$shopconfig->id}}">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                    <div class="modal fade" id="editShopEmail{{$shopconfig->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{__('Изменить электронную почту магазина')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="post" action="{{URL::to('/shop/storeShopEmail')}}">
                                    <div class="modal-body">
                                        <input type="hidden" value="{{$shopconfig->id}}" name="shopconfigId">
                                        <label for="shopConfigName">{{__('Электронная почта магазина')}}</label>
                                        <input type="text" class="form-control" value="{{$shopconfig->shopEmail}}" name="shopConfigEmail">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Закрыть')}}</button>
                                        <button type="submit" class="btn btn-primary">{{__('Сохранить электронную почту')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    @endif
@endsection
