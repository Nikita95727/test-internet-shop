@extends('layouts.admin.adminlayout')
@section('content')
    <form action="{{ URL::to('/shop/admin/managestores/store') }}" method="post">
        <div class="control-panel">
            <button type="submit" class="btn btn-success">{{ __('actions.save') }}</button>
        </div>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" id="store-tab" data-toggle="tab" href="#store" role="tab" aria-controls="store" aria-selected="true">{{ __('admin.store') }}</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="localization-tab" data-toggle="tab" href="#localization" role="tab" aria-controls="localization" aria-selected="false">{{ __('admin.localization') }}</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="images-tab" data-toggle="tab" href="#images" role="tab" aria-controls="contact" aria-selected="false">{{ __('admin.image') }}</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="store" role="tabpanel" aria-labelledby="store-tab">
                <div class="form-group">
                    <label for="storeName">{{ __('admin.store_name') }}</label>
                    <input type="text" class="form-control" name="storeName" value="{{ $store->name }}">
                </div>
                <div class="form-group">
                    <label for="storeAddress">{{ __('admin.store_address') }}</label>
                    <input type="text" class="form-control" name="storeAddress" value="{{ $store->getAddress() }}">
                </div>
                @foreach($store->getEmails() as $email)
                    <div class="form-group">
                        <label for="storeEmail">{{ __('admin.store_email') }}</label>
                        <input type="email" class="form-control" name="storeEmails[]" value="{{ $email->email }}">
                    </div>
                @endforeach
                @foreach($store->getPhones() as $phone)
                    <div class="form-group">
                        <label for="storePhone">{{ __('admin.store_phone') }}</label>
                        <input type="tel" class="form-control" name="storePhones[]" value="{{ $phone->phone }}">
                    </div>
                @endforeach
            </div>
            <div class="tab-pane fade" id="localization" role="tabpanel" aria-labelledby="localization-tab">
                <div class="form-group">
                    <x-locales-list/>
                </div>
                <div class="form-group">
                    <x-currencies-list/>
                </div>
            </div>
            <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
                <div class="form-group">
                    <label for="storeLogo">{{ __('admin.image') }}</label>
                    <input type="file" class="form-control" name="storeLogo">
                </div>
            </div>
        </div>
    </form>
@endsection
