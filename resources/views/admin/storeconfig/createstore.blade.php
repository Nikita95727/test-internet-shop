@extends('layouts.admin.adminlayout')
@section('content')
    <form action="{{ URL::to('/shop/admin/managestores/store') }}" method="post">
        <div class="control-panel">
            <button type="submit" class="btn btn-success">{{ __('actions.save') }}</button>
        </div>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" id="store-tab" data-toggle="tab" href="#store" role="tab" aria-controls="store" aria-selected="true">{{ __('admin.store') }}</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="localization-tab" data-toggle="tab" href="#localization" role="tab" aria-controls="localization" aria-selected="false">{{ __('admin.localization') }}</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="images-tab" data-toggle="tab" href="#images" role="tab" aria-controls="contact" aria-selected="false">{{ __('admin.image') }}</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="store" role="tabpanel" aria-labelledby="store-tab">
                <div class="form-group">
                    <label for="storeName">{{ __('admin.store_name') }}</label>
                    <input type="text" class="form-control" name="storeName">
                </div>
                <div class="form-group">
                    <label for="storeAddress">{{ __('admin.store_address') }}</label>
                    <input type="text" class="form-control" name="storeAddress">
                </div>
                <div class="form-group">
                    <label for="storeEmail">{{ __('admin.store_email') }}</label>
                    <input type="email" class="form-control" name="storeEmails[]">
                    <button type="button" class="btn btn-info" id="addEmailField">{{ __('actions.add_email') }}</button>
                    <script>
                        $('#addEmailField').click(function () {
                           $('#addEmailField').before('<label for="storeEmail">{{ __('admin.store_email') }}</label><input type="email" class="form-control" name="storeEmails[]">');
                        });
                    </script>
                </div>
                <div class="form-group">
                    <label for="storePhone">{{ __('admin.store_phone') }}</label>
                    <input type="tel" class="form-control" name="storePhones[]">
                    <button type="button" class="btn btn-info" id="addPhoneField">{{ __('actions.add_phone') }}</button>
                    <script>
                        $('#addPhoneField').click(function () {
                            $('#addPhoneField').before('<label for="storePhone">{{ __('admin.store_phone') }}</label><input type="tel" class="form-control" name="storePhones[]">');
                        });
                    </script>
                </div>
            </div>
            <div class="tab-pane fade" id="localization" role="tabpanel" aria-labelledby="localization-tab">
                <div class="form-group" id="localeList">
                    <x-locales-list/>
                </div>
                <button type="button" class="btn btn-info" id="addNewLocaleBtn">{{ __('admin.add_new_locale') }}</button>
                <script>
                    $('#addNewLocaleBtn').click(function () {
                        $('#locale-select').clone().appendTo('#localeList');
                    });
                </script>
                <div class="form-group" id="currencyList">
                    <div id="currency-select">
                        <x-currencies-list/>
                    </div>
                </div>
                <button type="button" class="btn btn-info" id="addNewCurrencyBtn">{{ __('admin.add_new_locale') }}</button>
                <script>
                    $('#addNewCurrencyBtn').click(function () {
                        $('#currency-select').clone().appendTo('#currencyList');
                    });
                </script>
            </div>
            <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
                <div class="form-group">
                    <label for="storeLogo">{{ __('admin.image') }}</label>
                    <input type="file" class="form-control" name="storeLogo">
                </div>
            </div>
        </div>
    </form>
@endsection
