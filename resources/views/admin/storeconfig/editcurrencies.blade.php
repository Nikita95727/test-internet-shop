@extends('layouts.admin.adminlayout')
@section('content')
    <div class="control-panel">
        <a href="#" class="btn btn-success" data-toggle="modal" data-target="#addLocaleModal">{{ __('admin.add_new_locale') }}</a>
        <div class="modal fade" id="addLocaleModal" tabindex="-1" role="dialog" aria-labelledby="addLocaleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addLocaleModalLabel">{{ __('admin.add_new_locale') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="{{URL::to('/shop/admin/managestores/addLocale')}}">
                        <div class="modal-body">
                            <input type="hidden" value="{{ $store->id }}" name="store_id">
                            <div id="locale-select">
                                <x-currencies-list/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('actions.close') }}</button>
                            <button type="submit" class="btn btn-primary">{{ __('actions.save') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @if($store->getCurrencies())
        <table class="table table-bordered">
            <thead>
            <th>{{ __('admin.currency_name') }}</th>
            <th>{{ __('admin.currency_code') }}</th>
            <th>{{ __('text.status') }}</th>
            <th>{{ __('actions.actions') }}</th>
            </thead>
            <tbody>
            @foreach($store->getCurrencies() as $storeCurrency)
                <tr>
                    <td>{{ $storeCurrency->getCurrencyName() }}</td>
                    <td>{{ $storeCurrency->getCurrencyCode() }}</td>
                    <td>
                        @if($storeCurrency->default)
                            {{ __('text.active') }}
                        @else
                            {{ __('text.not_active') }}
                        @endif
                    </td>
                    <td>
                        <a href="{{ URL::to('/shop/admin/managestores/deleteLocale/'.$storeCurrency->id.'/'.$store->id) }}" class="btn btn-danger">
                            <i class="fas fa-trash"></i>
                        </a>
                        @if($storeCurrency->default)
                            <a href="{{ URL::to('/shop/admin/managestores/changeCurrencyStatus/'.$storeCurrency->id.'/'.$store->id) }}" class="btn btn-success"><i class="fas fa-check-circle"></i></a>
                        @else
                            <a href="{{ URL::to('/shop/admin/managestores/changeCurrencyStatus/'.$storeCurrency->id.'/'.$store->id) }}" class="btn btn-light"><i class="fas fa-check-circle"></i></a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection
