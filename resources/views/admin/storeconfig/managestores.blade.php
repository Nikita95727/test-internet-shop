@extends('layouts.admin.adminlayout')
@section('content')
    <h3>{{ __('admin.store_management') }}</h3>
    <div class="control-panel">
        <a href="{{ URL::to('/shop/admin/managestores/create') }}" class="btn btn-success">{{ __('actions.create') }}</a>
    </div>
    @if($stores->isNotEmpty())
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>{{ __('admin.store_name') }}</th>
                    <th>{{ __('admin.default_store') }}</th>
                    <th>{{ __('actions.actions') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($stores as $store)
                    <tr>
                        <td>{{ $store->name }}</td>
                        <td>
                            @if($store->current)
                                <a href="{{ URL::to('/shop/admin/managestores/setCurrentStore/'.$store->id) }}" class="btn btn-success"><i class="fas fa-check-circle"></i></a>
                            @else
                                <a href="{{ URL::to('/shop/admin/managestores/setCurrentStore/'.$store->id) }}" class="btn btn-light"><i class="fas fa-check-circle"></i></a>
                            @endif
                        </td>
                        <td>
                            <a href="{{ URL::to('/shop/admin/managestores/delete/'.$store->id) }}" class="btn btn-danger">
                                <i class="fas fa-trash"></i>
                            </a>
                            <a href="{{ URL::to('/shop/admin/managestores/edit/'.$store->id) }}" class="btn btn-primary">
                                <i class="fas fa-edit"></i>
                            </a>
                            <a href="{{ URL::to('/shop/admin/managestores/editLocale/'.$store->id) }}" class="btn btn-dark" title="{{ __('actions.change_locales') }}">
                                <i class="fas fa-language"></i>
                            </a>
                            <a href="{{ URL::to('/shop/admin/managestores/editCurrencies/'.$store->id) }}" class="btn btn-success" title="{{ __('actions.change_currencies') }}">
                                <i class="fas fa-coins"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
@endsection
