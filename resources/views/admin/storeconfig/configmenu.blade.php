@extends('layouts.admin.adminlayout')
@section('content')
    <div class="action-item">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="{{URL::to('/admin/managelocale')}}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ __('admin.localization') }}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="action-item">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="{{URL::to('/shop/admin/managecurrencies')}}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ __('admin.currencies') }}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="action-item">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="{{URL::to('/shop/admin/managestores')}}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ __('admin.store_management') }}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
@endsection
