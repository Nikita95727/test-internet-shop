@extends('layouts.admin.adminlayout')
@section('content')
    <div class="row">
        <h3 class="admin-greeting-message">{{ __('admin.greeting_message') }}</h3>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-6 col-lg-4">
        <x-total-orders-qty/>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-6 col-lg-4">
        <x-total-users-qty/>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-6 col-lg-4">
        <x-total-sales/>
    </div>
@endsection
