window.onscroll = function () {
    myFunction()
};

var navbar = document.getElementById("fixed");
var sticky = navbar.offsetTop;

function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("fixed-menu")
    } else {
        navbar.classList.remove("fixed-menu");
    }
}
